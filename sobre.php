
<!DOCTYPE html>
<html>
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row">
				<div id="sobretxt" class="col s12 m6 l6 center">
					<h5><i class="small material-icons">info</i> Sobre o Sistema</h5>
					<p>O sistema de Escalação Automática para CartolaFC foi desenvolvido utilizando as linguagens PHP e Javascript/jQuery, 
					além de fazer uso da API do <i>fantasy game</i> CartolaFC.</p>
					<p>Para sugerir as escalações, o sistema considera diversos fatores, como por exemplo: pontuação e valorização 
					dos jogadores, média de gols por partida, quantidade de assistências, defesas difíceis, roubadas de bola, entre outros.</p>
					<p>O sistema está atualmente em fase de testes e desenvolvimento!
					<br />Por: <strong>Pedro Ferreira (Romenum)</strong></p>
				</div>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>