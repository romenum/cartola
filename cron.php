<?php

// ATLETAS MERCADO
$url_mercado = "https://api.cartolafc.globo.com/atletas/mercado";
$json_mercado = exec("curl -X GET ".$url_mercado);

$content_mercado = $json_mercado;
$fp_mercado = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/atletas_mercado.json","wb");
fwrite($fp_mercado,$content_mercado);
fclose($fp_mercado);


// STATUS MERCADO
$url_mercado_status = "https://api.cartolafc.globo.com/mercado/status";
$json_mercado_status = exec("curl -X GET ".$url_mercado_status);

$content_mercado_status = $json_mercado_status;
$fp_mercado_status = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/mercado_status.json","wb");
fwrite($fp_mercado_status,$content_mercado_status);
fclose($fp_mercado_status);


// CLUBES
$url_clubes = "https://api.cartolafc.globo.com/clubes";
$json_clubes = exec("curl -X GET ".$url_clubes);

$content_clubes = $json_clubes;
$fp_clubes = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/clubes.json","wb");
fwrite($fp_clubes,$content_clubes);
fclose($fp_clubes);


// PARTIDAS RODADA ATUAL
$array_mercado_status = json_decode($json_mercado_status);
$rodada_atual = $array_mercado_status->rodada_atual; // DECLARANDO A VARIÁVEL RODADA ATUAL USADA À FRENTE

$url_partidas_rodada_atual = "https://api.cartolafc.globo.com/partidas/" . $rodada_atual;
$json_partidas_rodada_atual = exec("curl -X GET ".$url_partidas_rodada_atual);

$content_partidas_rodada_atual = $json_partidas_rodada_atual;
$fp_partidas_rodada_atual = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/partidas_rodada_atual.json","wb");
fwrite($fp_partidas_rodada_atual,$content_partidas_rodada_atual);
fclose($fp_partidas_rodada_atual);


// PARCIAIS - ATLETAS PONTUADOS
$url_atletas_pontuados = "https://api.cartolafc.globo.com/atletas/pontuados";
$json_atletas_pontuados = exec("curl -X GET ".$url_atletas_pontuados);

$content_atletas_pontuados = $json_atletas_pontuados;
$fp_atletas_pontuados = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/atletas_pontuados.json","wb");
fwrite($fp_atletas_pontuados,$content_atletas_pontuados);
fclose($fp_atletas_pontuados);


// RODADAS
$url_rodadas = "https://api.cartolafc.globo.com/rodadas";
$json_rodadas = exec("curl -X GET ".$url_rodadas);
$array_rodadas = json_decode($json_rodadas);

foreach ($array_rodadas as $rodada) {
	if ($rodada->rodada_id == $rodada_atual) {
		$fimrodada = strtotime($rodada->fim);
	}
}

$agora = time();
$posrodada = date("Y-m-d H:i:s", strtotime($fimrodada . " +5 hours"));
if ($fimrodada < $agora && $agora < $posrodada) {
	$fp_atletas_pontuados_rodadaatual = fopen($_SERVER['DOCUMENT_ROOT'] . "/cartola/json/atletas_pontuados_rodada".$rodada_atual.".json","wb");
	fwrite($fp_atletas_pontuados_rodadaatual,$content_atletas_pontuados);
	fclose($fp_atletas_pontuados_rodadaatual);
}

echo '<h1 style="text-align: center; margin: 20px; padding: 20px; font-size: 32px; color: green;">ARQUIVOS BAIXADOS COM SUCESSO!</h1>';

?>