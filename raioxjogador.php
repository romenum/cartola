<?php
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';

include 'inc/json_partidas_rodada_atual.php';

if($_POST['jogadorid']) {
    $jogadorid = $_POST['jogadorid'];
	foreach ($array_mercado->atletas as $atletas_mercado) {
		if ($atletas_mercado->atleta_id == $jogadorid) {
			
			foreach ($array_clubes as $clubes) {
				if ($clubes->id == $atletas_mercado->clube_id) {
					$arr_clube = array (
						'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
					);
				}
			}
			echo '<h6 class="nome_atleta_raiox">
				<img src="'. $arr_clube["escudo_clube"] .'" alt="'. $arr_clube["nome_clube"] .'" title="'. $arr_clube["nome_clube"] .'" /> 
				' . $atletas_mercado->apelido . '
			</h6>';
			
			$qtde_rodadas = 4;
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				$i_zerofill = str_pad($i, 2, '0', STR_PAD_LEFT);
				${'arquivo_rodada'. $i_zerofill} = file_get_contents('db/2017/rodada'.$i_zerofill.'.json');
				${'array_rodada' . $i_zerofill} = json_decode(${'arquivo_rodada' . $i_zerofill});
				
				foreach (${'array_rodada' . $i_zerofill}->atletas as ${'rodada' . $i_zerofill}) {
					if (${'rodada' . $i_zerofill}->atleta_id == $jogadorid) {
						
						$arr_rodadas[$i] = $i . "ª rodada";
						if ($i == 1) {
							$arr_pontuacao[$i] = ${'rodada' . $i_zerofill}->media_num;
						} else {
							$arr_pontuacao[$i] = ${'rodada' . $i_zerofill}->pontos_num;
						}
						$arr_media[$i] = ${'rodada' . $i_zerofill}->media_num;
						$arr_precos[$i] = ${'rodada' . $i_zerofill}->preco_num;
						
						
						if (!empty(${'rodada' . $i_zerofill}->scout->RB)) {$scout_rb = ${'rodada' . $i_zerofill}->scout->RB;} else {$scout_rb = 0;}
						$arr_scout_defesa[$i]["RB"] = $scout_rb;
						if (!empty(${'rodada' . $i_zerofill}->scout->FC)) {$scout_fc = ${'rodada' . $i_zerofill}->scout->FC;} else {$scout_fc = 0;}
						$arr_scout_defesa[$i]["FC"] = $scout_fc;
						if (!empty(${'rodada' . $i_zerofill}->scout->GC)) {$scout_gc = ${'rodada' . $i_zerofill}->scout->GC;} else {$scout_gc = 0;}
						$arr_scout_defesa[$i]["GC"] = $scout_gc;
						if (!empty(${'rodada' . $i_zerofill}->scout->CA)) {$scout_ca = ${'rodada' . $i_zerofill}->scout->CA;} else {$scout_ca = 0;}
						$arr_scout_defesa[$i]["CA"] = $scout_ca;
						if (!empty(${'rodada' . $i_zerofill}->scout->CV)) {$scout_cv = ${'rodada' . $i_zerofill}->scout->CV;} else {$scout_cv = 0;}
						$arr_scout_defesa[$i]["CV"] = $scout_cv;
						if (!empty(${'rodada' . $i_zerofill}->scout->SG)) {$scout_sg = ${'rodada' . $i_zerofill}->scout->SG;} else {$scout_sg = 0;}
						$arr_scout_defesa[$i]["SG"] = $scout_sg;
						if (!empty(${'rodada' . $i_zerofill}->scout->DD)) {$scout_dd = ${'rodada' . $i_zerofill}->scout->DD;} else {$scout_dd = 0;}
						$arr_scout_defesa[$i]["DD"] = $scout_dd;
						if (!empty(${'rodada' . $i_zerofill}->scout->DP)) {$scout_dp = ${'rodada' . $i_zerofill}->scout->DP;} else {$scout_dp = 0;}
						$arr_scout_defesa[$i]["DP"] = $scout_dp;
						if (!empty(${'rodada' . $i_zerofill}->scout->GS)) {$scout_gs = ${'rodada' . $i_zerofill}->scout->GS;} else {$scout_gs = 0;}
						$arr_scout_defesa[$i]["GS"] = $scout_gs;
						
						if (!empty(${'rodada' . $i_zerofill}->scout->FS)) {$scout_fs = ${'rodada' . $i_zerofill}->scout->FS;} else {$scout_fs = 0;}
						$arr_scout_defesa[$i]["FS"] = $scout_fs;
						if (!empty(${'rodada' . $i_zerofill}->scout->PE)) {$scout_pe = ${'rodada' . $i_zerofill}->scout->PE;} else {$scout_pe = 0;}
						$arr_scout_defesa[$i]["PE"] = $scout_pe;
						if (!empty(${'rodada' . $i_zerofill}->scout->A)) {$scout_a = ${'rodada' . $i_zerofill}->scout->A;} else {$scout_a = 0;}
						$arr_scout_defesa[$i]["A"] = $scout_a;
						if (!empty(${'rodada' . $i_zerofill}->scout->FT)) {$scout_ft = ${'rodada' . $i_zerofill}->scout->FT;} else {$scout_ft = 0;}
						$arr_scout_defesa[$i]["FT"] = $scout_ft;
						if (!empty(${'rodada' . $i_zerofill}->scout->FD)) {$scout_fd = ${'rodada' . $i_zerofill}->scout->FD;} else {$scout_fd = 0;}
						$arr_scout_defesa[$i]["FD"] = $scout_fd;
						if (!empty(${'rodada' . $i_zerofill}->scout->FF)) {$scout_ff = ${'rodada' . $i_zerofill}->scout->FF;} else {$scout_ff = 0;}
						$arr_scout_defesa[$i]["FF"] = $scout_ff;
						if (!empty(${'rodada' . $i_zerofill}->scout->G)) {$scout_g = ${'rodada' . $i_zerofill}->scout->G;} else {$scout_g = 0;}
						$arr_scout_defesa[$i]["G"] = $scout_g;
						if (!empty(${'rodada' . $i_zerofill}->scout->I)) {$scout_i = ${'rodada' . $i_zerofill}->scout->I;} else {$scout_i = 0;}
						$arr_scout_defesa[$i]["I"] = $scout_i;
						if (!empty(${'rodada' . $i_zerofill}->scout->PP)) {$scout_pp = ${'rodada' . $i_zerofill}->scout->PP;} else {$scout_pp = 0;}
						$arr_scout_defesa[$i]["PP"] = $scout_pp;
						
					}
				}
			}
			
			include 'inc/grafico.php';
		}
	}
 }

?>