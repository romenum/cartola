<?php
include 'inc/funcoes.php';
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row">
				<div id="index_escalacao" class="col s12 m4 l4 center">
					<h5><i class="small material-icons">verified_user</i> Escalação Automática</h5>
					<p class="parag_principal">Preencha os campos do formulário e obtenha uma sugestão de escalação para seu time
					gerada de forma automática e baseada em I.A. (Inteligência Artificial).</p>
					<a href="escalacao" class="btn cyan darken-3">veja mais</a>
				</div>
				<div id="index_aovivo" class="col s12 m4 l4 center">
					<h5><i class="small material-icons">today</i> Parciais da Rodada</h5>
					<p class="parag_principal">Acompanhe as parciais dos jogadores em tempo real durante as rodadas do campeonato
					e veja informações de pontuação e scouts, por exemplo.</p>
					<a href="parciais" class="btn cyan darken-3">veja mais</a>
				</div>
				<div id="index_aovivo" class="col s12 m4 l4 center">
					<h5><i class="small material-icons">trending_up</i> Raio-X dos Atletas</h5>
					<p class="parag_principal">Veja a trajetória completa e detalhada de todos os jogadores, rodada a rodada, desde 
					o início do campeonato em uma espécie de raio-x dos atletas.</p>
					<a href="#" class="btn cyan darken-3">veja mais</a>
				</div>
			</div>
			<div class="row">
				<div id="index_mercado" class="col s12 m6 l3 center">
					<h5><i class="small material-icons">swap_vert</i> Status do Mercado</h5>
					<p><i class="small material-icons">repeat</i> Rodada Atual: <strong><?php echo $rodada_atual; ?>ª</strong></p>
					<p>
						<?php
							if ($status_mercado == 1) {
								echo '<i class="small material-icons">lock_open</i> ';
							} else {
								echo '<i class="small material-icons">lock_outline</i> ';
							}
						?>
						Mercado: <strong><?php echo $status_mercado_txt; ?></strong>
					</p>
					<?php
					if ($status_mercado == 1) {
						echo '<p><i class="small material-icons">alarm_on</i> Fecha em: <strong><span id="timer"></span></strong></p>';
					} else {
						echo '<p><i class="small material-icons">alarm_on</i> Aguarde o fim da rodada</p>';
					}
					?>
					
				</div>
				<div class="col s12 m6 l3 center">
					<h5><i class="small material-icons">schedule</i> Próximos Jogos</h5>
					<p class="grey-text">Em construção...</p>
				</div>
				<div class="col s12 m6 l3 center">
					<h5><i class="small material-icons">list</i> Tabela de Classificação</h5>
					<p class="grey-text">Em construção...</p>
				</div>
				<div id="artilheiros" class="col s12 m6 l3">
					<h5><i class="small material-icons">person_pin</i> Artilharia</h5>
					<div class="row">
						<div class="col s8">Ranking</div>
						<div class="col s4 txt_align_right">Gols</div>
					</div>
					<?php
					
					if ($status_mercado == 1) {
						foreach ($array_mercado->atletas as $atletas_mercado) {
							// FOTO DO ATLETA
							$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
							$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);
							
							$obj_artilheiros = new stdClass;
							$obj_artilheiros->atleta_id = $atletas_mercado->atleta_id;
							$obj_artilheiros->atleta_apelido = $atletas_mercado->apelido;
							$obj_artilheiros->atleta_foto_140 = $foto_atleta_140;
							$obj_artilheiros->atleta_foto_80 = $foto_atleta_80;
							$obj_artilheiros->atleta_clube = $atletas_mercado->clube_id;
							$obj_artilheiros->posicao_id = $atletas_mercado->posicao_id;
							if (!empty($atletas_mercado->scout->G)) {
								$obj_artilheiros->scout_gol = $atletas_mercado->scout->G;
							} else {
								$obj_artilheiros->scout_gol = 0;
							}
							if ($obj_artilheiros->scout_gol > 1) {
								$arr_artilheiros[] = $obj_artilheiros;
							}
						}
						
						usort($arr_artilheiros, "sortByGol");
						$contagem = 1;
						foreach ($arr_artilheiros as $artilheiros) {
							echo '<div class="row">';
								echo '<div class="col s1 txt_align_middle">' . $contagem . '</div>';
								echo '<div class="col s2 artilheiro_foto">
									<img alt="' . $artilheiros->atleta_apelido . '" title="' . $artilheiros->atleta_apelido . '" 
									src="' . $artilheiros->atleta_foto_80 . '" />
								</div>';
								foreach ($array_clubes as $clubes) {
									if ($clubes->id == $artilheiros->atleta_clube) {
										$arr_clube = array (
											'nome_clube' => $clubes->nome, 'escudo_clube' => end($clubes->escudos)
										);
										echo '<div class="col s1 artilheiro_clube">
											<img alt="' . $arr_clube['nome_clube'] . '" title="' . $arr_clube['nome_clube'] . '" 
											src="' . $arr_clube['escudo_clube'] . '" />
										</div>';
									}
								}
								echo '<div class="col s6 artilheiro_apelido">' . $artilheiros->atleta_apelido . ' <br /> ' . 
								${'posicao' . $artilheiros->posicao_id} . '</div>';
								echo '<div class="col s2 txt_align_right txt_align_middle">' . $artilheiros->scout_gol . '</div>';
							echo '</div>';
							$contagem++;
						}
					} else {
						echo '<p id="art_mercado_fechado" class="card center yellow lighten-2 red-text text-darken-2">As informações de 
						artilharia serão atualizadas quando o mercado abrir novamente.</p>';
					}
					?>
				</div>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>