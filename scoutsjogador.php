<?php
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';

//include 'inc/json_atletas_pontuados.php';
$url_atletas_pontuados = "https://api.cartolafc.globo.com/atletas/pontuados";
$json_atletas_pontuados = exec("curl -X GET ".$url_atletas_pontuados);
$array_atletas_pontuados = json_decode($json_atletas_pontuados);

$ext_counter = 1;
foreach ($array_atletas_pontuados->atletas as $atletas_key => $atletas_value) {
	$counter = 1;
	foreach ($atletas_value as $atletas_value_k => $atletas_value_v) {
		$atletas_pontuados_int["id"] =  $atletas_key;
		if ($counter == 1) {
			$atletas_pontuados_int["nome"] =  $atletas_value_v;
		} else if ($counter == 6) {
			$atletas_pontuados_int["clube"] =  $atletas_value_v;
		} else if ($counter == 5) {
			$atletas_pontuados_int["posicao"] =  $atletas_value_v;
		} else if ($counter == 2) {
			$atletas_pontuados_int["pontuacao"] =  $atletas_value_v;
		} else if ($counter == 3) {
			if (!empty($atletas_value_v->RB)) {$scout_rb = $atletas_value_v->RB;} else {$scout_rb = 0;}
			$atletas_pontuados_int["RB"] = $scout_rb;
			if (!empty($atletas_value_v->FC)) {$scout_fc = $atletas_value_v->FC;} else {$scout_fc = 0;}
			$atletas_pontuados_int["FC"] = $scout_fc;
			if (!empty($atletas_value_v->GC)) {$scout_gc = $atletas_value_v->GC;} else {$scout_gc = 0;}
			$atletas_pontuados_int["GC"] = $scout_gc;
			if (!empty($atletas_value_v->CA)) {$scout_ca = $atletas_value_v->CA;} else {$scout_ca = 0;}
			$atletas_pontuados_int["CA"] = $scout_ca;
			if (!empty($atletas_value_v->CV)) {$scout_cv = $atletas_value_v->CV;} else {$scout_cv = 0;}
			$atletas_pontuados_int["CV"] = $scout_cv;
			if (!empty($atletas_value_v->SG)) {$scout_sg = $atletas_value_v->SG;} else {$scout_sg = 0;}
			$atletas_pontuados_int["SG"] = $scout_sg;
			if (!empty($atletas_value_v->DD)) {$scout_dd = $atletas_value_v->DD;} else {$scout_dd = 0;}
			$atletas_pontuados_int["DD"] = $scout_dd;
			if (!empty($atletas_value_v->DP)) {$scout_dp = $atletas_value_v->DP;} else {$scout_dp = 0;}
			$atletas_pontuados_int["DP"] = $scout_dp;
			if (!empty($atletas_value_v->GS)) {$scout_gs = $atletas_value_v->GS;} else {$scout_gs = 0;}
			$atletas_pontuados_int["GS"] = $scout_gs;
			
			if (!empty($atletas_value_v->FS)) {$scout_fs = $atletas_value_v->FS;} else {$scout_fs = 0;}
			$atletas_pontuados_int["FS"] = $scout_fs;
			if (!empty($atletas_value_v->PE)) {$scout_pe = $atletas_value_v->PE;} else {$scout_pe = 0;}
			$atletas_pontuados_int["PE"] = $scout_pe;
			if (!empty($atletas_value_v->A)) {$scout_a = $atletas_value_v->A;} else {$scout_a = 0;}
			$atletas_pontuados_int["A"] = $scout_a;
			if (!empty($atletas_value_v->FT)) {$scout_ft = $atletas_value_v->FT;} else {$scout_ft = 0;}
			$atletas_pontuados_int["FT"] = $scout_ft;
			if (!empty($atletas_value_v->FD)) {$scout_fd = $atletas_value_v->FD;} else {$scout_fd = 0;}
			$atletas_pontuados_int["FD"] = $scout_fd;
			if (!empty($atletas_value_v->FF)) {$scout_ff = $atletas_value_v->FF;} else {$scout_ff = 0;}
			$atletas_pontuados_int["FF"] = $scout_ff;
			if (!empty($atletas_value_v->G)) {$scout_g = $atletas_value_v->G;} else {$scout_g = 0;}
			$atletas_pontuados_int["G"] = $scout_g;
			if (!empty($atletas_value_v->I)) {$scout_i = $atletas_value_v->I;} else {$scout_i = 0;}
			$atletas_pontuados_int["I"] = $scout_i;
			if (!empty($atletas_value_v->PP)) {$scout_pp = $atletas_value_v->PP;} else {$scout_pp = 0;}
			$atletas_pontuados_int["PP"] = $scout_pp;
		}
		$counter++;
	}
	$ext_counter++;
	$atletas_pontuados_ext[] = $atletas_pontuados_int;
}

//print_r($atletas_pontuados_ext);
//echo '<hr>';

if($_POST['jogadorid']) {
    $jogadorid = $_POST['jogadorid'];
	foreach ($atletas_pontuados_ext as $atletas_pontuados) {
		//echo $atletas_pontuados["id"] . '<hr>';
		if ($atletas_pontuados["id"] == $jogadorid) {
			//echo 'aqui -> ' . $atletas_pontuados["id"] . '<hr>';
			?>
			
			<div class="row">
				<div class="col s12 m6 l6">
					<h6>Scouts de defesa</h6>
					<table>
						<tbody>
							<tr style="color: green;"><td title="Defesa de Pênalti">DP (+7,0)</td><td><?php echo $atletas_pontuados["DP"]; ?></td></tr>
							<tr style="color: green;"><td title="Saldo de Gols">SG (+5,0)</td><td><?php echo $atletas_pontuados["SG"]; ?></td></tr>
							<tr style="color: green;"><td title="Defesa Difícil">DD (+3,0)</td><td><?php echo $atletas_pontuados["DD"]; ?></td></tr>
							<tr style="color: green;"><td title="Roubada de Bola">RB (+1,7)</td><td><?php echo $atletas_pontuados["RB"]; ?></td></tr>
							<tr style="color: red;"><td title="Falta Cometida">FC (-0,5)</td><td><?php echo $atletas_pontuados["FC"]; ?></td></tr>
							<tr style="color: red;"><td title="Cartão Amarelo">CA (-2,0)</td><td><?php echo $atletas_pontuados["CA"]; ?></td></tr>
							<tr style="color: red;"><td title="Gol Sofrido">GS (-2,0)</td><td><?php echo $atletas_pontuados["GS"]; ?></td></tr>
							<tr style="color: red;"><td title="Cartão Vermelho">CV (-5,0)</td><td><?php echo $atletas_pontuados["CV"]; ?></td></tr>
							<tr style="color: red;"><td title="Gol Contra">GC (-6,0)</td><td><?php echo $atletas_pontuados["GC"]; ?></td></tr>
						</tbody>
					</table>
				</div>
				<div class="col s12 m6 l6">
					<h6>Scouts de ataque</h6>
					<table>
						<tbody>
							<tr style="color: green;"><td title="Gol">G (+8,0)</td><td><?php echo $atletas_pontuados["G"]; ?></td></tr>
							<tr style="color: green;"><td title="Assistência">A (+5,0)</td><td><?php echo $atletas_pontuados["A"]; ?></td></tr>
							<tr style="color: green;"><td title="Finalização na Trave">FT (+3,5)</td><td><?php echo $atletas_pontuados["FT"]; ?></td></tr>
							<tr style="color: green;"><td title="Finalização Defendida">FD (+1,0)</td><td><?php echo $atletas_pontuados["FD"]; ?></td></tr>
							<tr style="color: green;"><td title="Finalização para Fora">FF (+0,7)</td><td><?php echo $atletas_pontuados["FF"]; ?></td></tr>
							<tr style="color: green;"><td title="Falta Sofrida">FS (+0,5)</td><td><?php echo $atletas_pontuados["FS"]; ?></td></tr>
							<tr style="color: red;"><td title="Passe Errado">PE (-0,3)</td><td><?php echo $atletas_pontuados["PE"]; ?></td></tr>
							<tr style="color: red;"><td title="Impedimento">I (-0,5)</td><td><?php echo $atletas_pontuados["I"]; ?></td></tr>
							<tr style="color: red;"><td title="Pênalti Perdido">PP (-3,5)</td><td><?php echo $atletas_pontuados["PP"]; ?></td></tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col s12 center">
					<a href="#" onclick="location.reload(true); return false;" id="scouts_atualizar" 
					class="card center yellow lighten-2 red-text text-darken-2">
						Para ver scouts mais atuais, atualize a página.</a>
				</div>
			</div>
			<?php
		}
	}
 }

?>