<?php

function pegaValor($valor) {
    return isset($_POST[$valor]) ? $_POST[$valor] : '';
}

function validaEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function enviaEmail($de, $nome, $assunto, $mensagem, $para) {
	$headers_original = "Content-Type:text/html; charset=UTF-8\n";
	$headers_original .= "From: ".$de."\n";
	$headers_original .= "Reply-To: " . $nome . "<".$de.">\n";
	$headers_original .= "X-Sender: <".$de.">\n";
	$headers_original .= "X-Mailer: PHP v".phpversion()."\n";
	$headers_original .= "X-IP: ".$_SERVER['REMOTE_ADDR']."\n";
	$headers_original .= "Return-Path: <".$de.">\n";
	$headers_original .= "MIME-Version: 1.0\n";
	$headers = utf8_decode($headers_original); /** HEADERS **/
	mail($para, $assunto, $mensagem, $headers); /** ENVIO DE EMAIL **/
}

$para = "romenum@gmail.com";
$nome = pegaValor("nome");
$de = pegaValor("email");
$mensagem = pegaValor("mensagem");
$assunto = "Contato via website (Cartola)";

if ($nome && validaEmail($de) && $mensagem) {
    enviaEmail($de, $nome, $assunto, $mensagem, $para);
    $pagina = "mailok.php";
} else {
    $pagina = "mailerror.php";
}

header("location:".$pagina);

?>