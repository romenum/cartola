<?php
include 'inc/funcoes.php';
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';

$patrimonioatual = $reservapatrimonio = $tipoformacao = $invest_areacampo = $perfil = $objetivo = "";

//--------------- TRATAMENTO DE FORM POST - INÍCIO ---------------//
if (isset($_POST["sugerirformacao"])) {
	
	$patrimonioatual = $_POST["patrimonio-atual"];
	$reservapatrimonio = $_POST["reserva-patrimonio"];
	$tipoformacao = $_POST["tipo_formacao"];
	$invest_areacampo = $_POST["invest_areacampo"];
	$perfil = $_POST["perfil"];
	$objetivo = $_POST["objetivo"];
	
	$investimento = $patrimonioatual - $patrimonioatual * ($reservapatrimonio / 100);
	$tipoformacao = str_replace("formacao", "", $tipoformacao);
	
	//---- Analisa as 7 formações e redistribui os pesos dos jogadores ----//
	//---- de acordo com a escolha feita no campo "Maior investimento" ----//
	for ($i=1; $i<8; $i++) {
		if ($tipoformacao == $i) {
			if ($invest_areacampo == 1) {
				$taxa_equilibrio = ($taxa_area_invest * $f1_qtde_def) / (11 - ${'f'.$i.'_qtde_def'});
				$peso_jog_def = $peso_jog_def + $peso_jog_def*$taxa_area_invest; // INVESTIR
				$peso_jog_mei = $peso_jog_mei - $peso_jog_mei*$taxa_equilibrio; // EQUILIBRAR
				$peso_jog_ata = $peso_jog_ata - $peso_jog_ata*$taxa_equilibrio; // EQUILIBRAR
			} else if ($invest_areacampo == 2) {
				$taxa_equilibrio = ($taxa_area_invest * $f1_qtde_mei) / (11 - ${'f'.$i.'_qtde_mei'});
				$peso_jog_def = $peso_jog_def - $peso_jog_def*$taxa_equilibrio; // EQUILIBRAR
				$peso_jog_mei = $peso_jog_mei + $peso_jog_mei*$taxa_area_invest; // INVESTIR
				$peso_jog_ata = $peso_jog_ata - $peso_jog_ata*$taxa_equilibrio; // EQUILIBRAR
			} else if ($invest_areacampo == 3) {
				$taxa_equilibrio = ($taxa_area_invest * ${'f'.$i.'_qtde_ata'}) / (11 - ${'f'.$i.'_qtde_ata'});
				$peso_jog_def = $peso_jog_def - $peso_jog_def*$taxa_equilibrio; // EQUILIBRAR
				$peso_jog_mei = $peso_jog_mei - $peso_jog_mei*$taxa_equilibrio; // EQUILIBRAR
				$peso_jog_ata = $peso_jog_ata + $peso_jog_ata*$taxa_area_invest; // INVESTIR
			} else if ($invest_areacampo == 4) {
				// PADRÃO
			} else {echo $var_erro_escolha_maior_invest;}
		}
	}
	include 'inc/escalacao.php';
}
//--------------- TRATAMENTO DE FORM POST - FIM ---------------//
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>

		<div id="principal">
			<div class="row">
				<?php
				if (!isset($_POST["sugerirformacao"])) {
					echo '<div id="coluna1" class="col s12 m6 l4">';
						include 'inc/formulario.php';
					echo '</div>';
				} else {
					?>
					<div id="coluna1" class="col s12 m6 l4 coluna1result">
						<h5><?php echo $var_dados_preenchidos; ?></h5>
						
						<?php
						$formacaoescolhidatxt = $invest_areacampotxt = $perfiltxt = $objetivotxt = "";
						
						$patrimonio_atual = number_format((float)($_POST["patrimonio-atual"]), 2, '.', '');
						$reserva_patrimonio = $_POST["reserva-patrimonio"];
						$patrimonio_investido = $patrimonio_atual - $patrimonio_atual * ($reserva_patrimonio / 100 );
						$patrimonio_investido = number_format((float)$patrimonio_investido, 2, '.', '');
						$patrimonio_economizado = $patrimonio_atual - $patrimonio_investido;
						$patrimonio_economizado = number_format((float)$patrimonio_economizado, 2, '.', '');
						
						$formacaoescolhida = $_POST["tipo_formacao"];
						
						if ($formacaoescolhida == 'formacao1') {$formacaoescolhidatxt = $formacao1;}
						else if ($formacaoescolhida == 'formacao2') {$formacaoescolhidatxt = $formacao2;}
						else if ($formacaoescolhida == 'formacao3') {$formacaoescolhidatxt = $formacao3;}
						else if ($formacaoescolhida == 'formacao4') {$formacaoescolhidatxt = $formacao4;}
						else if ($formacaoescolhida == 'formacao5') {$formacaoescolhidatxt = $formacao5;}
						else if ($formacaoescolhida == 'formacao6') {$formacaoescolhidatxt = $formacao6;}
						else if ($formacaoescolhida == 'formacao7') {$formacaoescolhidatxt = $formacao7;}
						
						if ($invest_areacampo == 1) {$invest_areacampotxt = $var_area_defesa;}
						else if ($invest_areacampo == 2) {$invest_areacampotxt = $var_area_meiocampo;}
						else if ($invest_areacampo == 3) {$invest_areacampotxt = $var_area_ataque;}
						else if ($invest_areacampo == 4) {$invest_areacampotxt = $var_area_igual;}
						
						if ($perfil == 1) {$perfiltxt = $var_perfil_conservador;}
						//else if ($perfil == 2) {$perfiltxt = $var_perfil_equilibrado;}
						else if ($perfil == 2) {$perfiltxt = $var_perfil_apostador;}
						
						if ($objetivo == 1) {$objetivotxt = $var_objetivo_valorizacao;}
						else if ($objetivo == 2) {$objetivotxt = $var_objetivo_pontuacao;}
						?>
						
						<p><i class="small material-icons">verified_user</i> <?php echo $var_patrimonio_atual; ?> 
						<strong>C$ <?php echo $patrimonio_atual; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_valor_para_invest; ?> 
						<strong>C$ <?php echo $patrimonio_investido; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_valor_economizado; ?> 
						<strong>C$ <?php echo $patrimonio_economizado; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_formacao_escolhida; ?> 
						<strong><?php echo $formacaoescolhidatxt; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_maior_investimento; ?> 
						<strong><?php echo $invest_areacampotxt; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_meu_perfil2; ?> 
						<strong><?php echo $perfiltxt; ?></strong></p>
						<p><i class="small material-icons">verified_user</i> <?php echo $var_objetivo; ?> 
						<strong><?php echo $objetivotxt; ?></strong></p>
						
						<div class="row center">
							<a href="#" data-activates="novaescalacao" id="bt_novaescalacao" class="novaescalacao btn cyan darken-3">
								<?php echo $var_alterar_dados; ?>
								<i class="material-icons right">loop</i>
							</a>
						</div>
						
						<div id="novaescalacao" class="side-nav teal lighten-5 teal-text text-darken-2">
							<?php include 'inc/formulario.php'; ?>
						</div>
					</div>
					<?php
				}
				?>

				<div id="coluna2" class="col s12 m6 l4">		
					<h5><?php echo $var_escalacao_campo; ?></h5>
					<?php
					if (!isset($_POST["sugerirformacao"])) {
						echo '<div id="formacao_campo" class="formacao0">
							<div id="invest_defesa" class="dinheiro0"></div>
							<div id="invest_meiocampo" class="dinheiro0"></div>
							<div id="invest_ataque" class="dinheiro0"></div>';
							for ($i=1; $i<13; $i++) {
								echo '<div id="jogador' . $i . '"></div>';
							}
						echo '</div>';
					} else {
						echo '<div id="formacao_campo_ok" class="formacao'.$tipoformacao.'">
							<div id="invest_defesa" class="dinheiro0';
							if ($invest_areacampo == 1 || $invest_areacampo == 4) {echo ' maisdinheiro';}
							echo '"></div>
							<div id="invest_meiocampo" class="dinheiro0';
							if ($invest_areacampo == 2 || $invest_areacampo == 4) {echo ' maisdinheiro';}
							echo '"></div>
							<div id="invest_ataque" class="dinheiro0';
							if ($invest_areacampo == 3 || $invest_areacampo == 4) {echo ' maisdinheiro';}
							echo '"></div>';
							
							include 'inc/mapa.php';
						echo '</div>';
					}
					?>
				</div>
				
				<div id="coluna3" class="col s12 m6 l4">
					<h5><?php echo $var_escalacao_lista; ?></h5>
					<?php
					if (!isset($_POST["sugerirformacao"])) {
						echo '<p class="card yellow lighten-2 red-text text-darken-2">
						Primeiramente, preencha o formulário para obter uma sugestão de esquema tático.</p>';
					} else {
						echo '<div id="formacao_lista">';
							
							include 'inc/lista.php';
						echo '</div>';
					}
					?>
				</div>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
		
	</body>
</html>
