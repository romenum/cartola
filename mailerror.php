<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row center">
				<h5>Erro</h5>			
				<p>Houve um erro no envio do e-mail. <a href="contato">Tentar novamente</a>.</p>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>