<?php
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row">
				<div id="raiox" class="col s12 m6 l6 center">
					<h5><i class="small material-icons">trending_up</i> Raio-X</h5>
					<p class="grey-text">Em construção...</p>
				</div>
			</div>
			
			<?php
			/*
			<div id="tabela_raiox_container" class="row">
				<table id="tabela_raiox" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th class="th_input">Jogador</th>
							<th class="th_input">Time</th>
							<th class="th_input">Posição</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Jogador</th>
							<th>Time</th>
							<th>Posição</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
						
						foreach ($array_mercado->atletas as $atletas_mercado) {
							echo '<tr>';
								echo '<td>
									<a class="modal-trigger" href="#modal_raiox" data-toggle="modal" data-id="' . $atletas_mercado->atleta_id . '">
										' . $atletas_mercado->apelido . '
									</a>
								</td>';
								
								foreach ($array_clubes as $clubes) {
									if ($clubes->id == $atletas_mercado->clube_id) {
										$arr_clube = array (
											'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
										);
									}
								}
								echo '<td>' . $arr_clube['nome_clube'] . '</td>';
								echo '<td>' . ${'posicao' . $atletas_mercado->posicao_id} . '</td>';
							echo '</tr>';
						}
						
						?>
					</tbody>
				</table>
			</div>
			<div id="modal_raiox" class="modal modal-fixed-footer">
				<div class="modal-header">
					<h5 class="left">Dados do jogador</h5>
				</div>
				<div class="modal-content">
					<ul class="tabs">
						<li class="tab col s4"><a id="firstTab" class="active" href="#tab1">Pontuação</a></li>
						<li class="tab col s4"><a href="#tab2">Cartoletas</a></li>
						<li class="tab col s4"><a href="#tab3">Scouts</a></li>
					</ul>
					<div class="fetched-data-raiox"></div>
				</div>
					<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn btn-flat">Fechar</a>
				</div>
			</div>
			*/
			?>
			
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>