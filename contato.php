<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row">
				<h5><i class="small material-icons">email</i> Formulário de Contato</h5>
				<form id="form_contato" action="mailsend.php" method="post" class="col s12 m6 l6">
					<p>Preencha os campos do formulário abaixo e clique em "Enviar" para entrar em contato com 
					o desenvolvedor do sistema. Deixe suas dúvidas, críticas, sugestões e/ou elogios.</p>
					<div class="input-field">
						<input name="nome" type="text" class="validate" required="" aria-required="true">
						<label for="nome">Nome: </label>
					</div>
					<div class="input-field">
						<input name="email" type="email" class="validate" required="" aria-required="true">
						<label for="email">E-mail: </label>
					</div>
					<div class="input-field">
						<textarea name="mensagem" class="materialize-textarea validate" required="" aria-required="true"></textarea>
						<label for="mensagem">Mensagem: </label>
					</div>
					<div class="row">
						<button class="btn waves-light cyan darken-3" type="submit">Enviar</button>
					</div>
				</form>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>