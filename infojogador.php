<?php
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
include 'inc/json_clubes.php';

include 'inc/json_partidas_rodada_atual.php';

if($_POST['jogadorid']) {
    $jogadorid = $_POST['jogadorid'];
	foreach ($array_mercado->atletas as $atletas_mercado) {
		if ($atletas_mercado->atleta_id == $jogadorid) {
			if (!empty($atletas_mercado->scout->RB)) {$scout_rb = $atletas_mercado->scout->RB;} else {$scout_rb = 0;}
			if (!empty($atletas_mercado->scout->FC)) {$scout_fc = $atletas_mercado->scout->FC;} else {$scout_fc = 0;}
			if (!empty($atletas_mercado->scout->GC)) {$scout_gc = $atletas_mercado->scout->GC;} else {$scout_gc = 0;}
			if (!empty($atletas_mercado->scout->CA)) {$scout_ca = $atletas_mercado->scout->CA;} else {$scout_ca = 0;}
			if (!empty($atletas_mercado->scout->CV)) {$scout_cv = $atletas_mercado->scout->CV;} else {$scout_cv = 0;}
			if (!empty($atletas_mercado->scout->SG)) {$scout_sg = $atletas_mercado->scout->SG;} else {$scout_sg = 0;}
			if (!empty($atletas_mercado->scout->DD)) {$scout_dd = $atletas_mercado->scout->DD;} else {$scout_dd = 0;}
			if (!empty($atletas_mercado->scout->DP)) {$scout_dp = $atletas_mercado->scout->DP;} else {$scout_dp = 0;}
			if (!empty($atletas_mercado->scout->GS)) {$scout_gs = $atletas_mercado->scout->GS;} else {$scout_gs = 0;}
			
			if (!empty($atletas_mercado->scout->FS)) {$scout_fs = $atletas_mercado->scout->FS;} else {$scout_fs = 0;}
			if (!empty($atletas_mercado->scout->PE)) {$scout_pe = $atletas_mercado->scout->PE;} else {$scout_pe = 0;}
			if (!empty($atletas_mercado->scout->A)) {$scout_a = $atletas_mercado->scout->A;} else {$scout_a = 0;}
			if (!empty($atletas_mercado->scout->FT)) {$scout_ft = $atletas_mercado->scout->FT;} else {$scout_ft = 0;}
			if (!empty($atletas_mercado->scout->FD)) {$scout_fd = $atletas_mercado->scout->FD;} else {$scout_fd = 0;}
			if (!empty($atletas_mercado->scout->FF)) {$scout_ff = $atletas_mercado->scout->FF;} else {$scout_ff = 0;}
			if (!empty($atletas_mercado->scout->G)) {$scout_g = $atletas_mercado->scout->G;} else {$scout_g = 0;}
			if (!empty($atletas_mercado->scout->I)) {$scout_i = $atletas_mercado->scout->I;} else {$scout_i = 0;}
			if (!empty($atletas_mercado->scout->PP)) {$scout_pp = $atletas_mercado->scout->PP;} else {$scout_pp = 0;}
			
			$txt_status = $txt_extenso_status = $color_status = "";
			if ($atletas_mercado->status_id == 7) {
				$txt_status = $txt_status_id7; $txt_extenso_status = $status_id7; $color_status = $color_status_id7;
			} else if ($atletas_mercado->status_id == 2) {
				$txt_status = $txt_status_id2; $txt_extenso_status = $status_id2; $color_status = $color_status_id2;
			} else {
				$color_status = 'inherit';
			}
			
			foreach ($array_partidas_rodada_atual->partidas as $arr_partidas) {
				if ($arr_partidas->clube_casa_id == $atletas_mercado->clube_id || $arr_partidas->clube_visitante_id == $atletas_mercado->clube_id) {
					$clube_casa_id = $arr_partidas->clube_casa_id;
					$clube_visitante_id = $arr_partidas->clube_visitante_id;
				}
			}
			
			foreach ($array_clubes as $clubes) {
				if ($clubes->id == $atletas_mercado->clube_id) {
					$jogador_clube = $clubes->nome;
					$jogador_clube_escudo = end($clubes->escudos);
				}
				
				if ($clubes->id == $clube_casa_id) {
					$clube_casa_nome = $clubes->nome;
					$clube_casa_escudo = end($clubes->escudos);
				}
				if ($clubes->id == $clube_visitante_id) {
					$clube_visit_nome = $clubes->nome;
					$clube_visit_escudo = end($clubes->escudos);
				}
			}
			
			$posicaotxt = "";
			for ($i=1; $i<=6; $i++) {
				if ($atletas_mercado->posicao_id == $i) {
					$posicaotxt = ${'posicao' . $i . 'txt'};
				}
			}
			
			if ($atletas_mercado->preco_num == 0) {$color_preco = "";}
			else if ($atletas_mercado->preco_num > 0) {$color_preco = ' style="color: green;"';} 
			else if ($atletas_mercado->preco_num < 0) {$color_preco = ' style="color: red;"';}
			
			if ($atletas_mercado->variacao_num == 0) {$color_var = "";}
			else if ($atletas_mercado->variacao_num > 0) {$color_var = ' style="color: green;"';} 
			else if ($atletas_mercado->variacao_num < 0) {$color_var = ' style="color: red;"';}
			
			if ($atletas_mercado->pontos_num == 0) {$color_pontu = "";}
			else if ($atletas_mercado->pontos_num > 0) {$color_pontu = ' style="color: green;"';} 
			else if ($atletas_mercado->pontos_num < 0) {$color_pontu = ' style="color: red;"';}
			
			if ($atletas_mercado->pontos_num == 0) {$color_media = "";}
			else if ($atletas_mercado->pontos_num > 0) {$color_media = ' style="color: green;"';} 
			else if ($atletas_mercado->pontos_num < 0) {$color_media = ' style="color: red;"';}
			?>
			
			<div class="row">
				<div class="col s12 m6 l6">
					<h6>Scouts básicos</h6>
					<table>
						<tbody>
							<tr><td>Nome</td><td><?php echo $atletas_mercado->apelido; ?></td></tr>
							<tr>
								<td>Time</td>
								<td>
									<img class="escudo_confronto" src="<?php echo $jogador_clube_escudo; ?>" 
									alt="<?php echo $jogador_clube; ?>" title="<?php echo $jogador_clube; ?>" />
									<?php echo ' ' . $jogador_clube; ?>
								</td>
							</tr>
							<tr><td>Posição</td><td><?php echo $posicaotxt; ?></td></tr>
							<tr>
								<td>Status</td>
								<td style="color: <?php echo $color_status; ?>;">
									<?php echo $txt_status; ?><?php echo ' ' . $txt_extenso_status; ?>
								</td>
							</tr>
							<tr><td>Preço</td><td><span<?php echo $color_preco; ?>><?php echo $atletas_mercado->preco_num; ?></span></td></tr>
							<tr><td>Variação (C$)</td><td><span<?php echo $color_var; ?>><?php echo $atletas_mercado->variacao_num; ?></span></td></tr>
							<tr><td>Últ. Pontuação (C$)</td><td><span<?php echo $color_pontu; ?>><?php echo $atletas_mercado->pontos_num; ?></span></td></tr>
							<tr><td>Média</td><td><span<?php echo $color_media; ?>><?php echo $atletas_mercado->media_num; ?></span></td></tr>
							<tr><td>Jogos</td><td><?php echo $atletas_mercado->jogos_num; ?></td></tr>
							<tr>
								<td>Confronto</td>
								<td><img class="escudo_confronto" src="<?php echo $clube_casa_escudo; ?>" 
								alt="<?php echo $clube_casa_nome; ?>" title="<?php echo $clube_casa_nome; ?>" /> x 
								<img class="escudo_confronto" src="<?php echo $clube_visit_escudo; ?>" alt="<?php echo $clube_visit_nome; ?>" 
								title="<?php echo $clube_visit_nome; ?>" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col s12 m3 l3">
					<h6>Scouts de defesa</h6>
					<table>
						<tbody>
							<tr style="color: green;"><td title="Defesa de Pênalti">DP (+7,0)</td><td><?php echo $scout_dp; ?></td></tr>
							<tr style="color: green;"><td title="Saldo de Gols">SG (+5,0)</td><td><?php echo $scout_sg; ?></td></tr>
							<tr style="color: green;"><td title="Defesa Difícil">DD (+3,0)</td><td><?php echo $scout_dd; ?></td></tr>
							<tr style="color: green;"><td title="Roubada de Bola">RB (+1,7)</td><td><?php echo $scout_rb; ?></td></tr>
							<tr style="color: red;"><td title="Falta Cometida">FC (-0,5)</td><td><?php echo $scout_fc; ?></td></tr>
							<tr style="color: red;"><td title="Cartão Amarelo">CA (-2,0)</td><td><?php echo $scout_ca; ?></td></tr>
							<tr style="color: red;"><td title="Gol Sofrido">GS (-2,0)</td><td><?php echo $scout_gs; ?></td></tr>
							<tr style="color: red;"><td title="Cartão Vermelho">CV (-5,0)</td><td><?php echo $scout_cv; ?></td></tr>
							<tr style="color: red;"><td title="Gol Contra">GC (-6,0)</td><td><?php echo $scout_gc; ?></td></tr>
						</tbody>
					</table>
				</div>
				<div class="col s12 m3 l3">
					<h6>Scouts de ataque</h6>
					<table>
						<tbody>
							<tr style="color: green;"><td title="Gol">G (+8,0)</td><td><?php echo $scout_g; ?></td></tr>
							<tr style="color: green;"><td title="Assistência">A (+5,0)</td><td><?php echo $scout_a; ?></td></tr>
							<tr style="color: green;"><td title="Finalização na Trave">FT (+3,5)</td><td><?php echo $scout_ft; ?></td></tr>
							<tr style="color: green;"><td title="Finalização Defendida">FD (+1,0)</td><td><?php echo $scout_fd; ?></td></tr>
							<tr style="color: green;"><td title="Finalização para Fora">FF (+0,7)</td><td><?php echo $scout_ff; ?></td></tr>
							<tr style="color: green;"><td title="Falta Sofrida">FS (+0,5)</td><td><?php echo $scout_fs; ?></td></tr>
							<tr style="color: red;"><td title="Passe Errado">PE (-0,3)</td><td><?php echo $scout_pe; ?></td></tr>
							<tr style="color: red;"><td title="Impedimento">I (-0,5)</td><td><?php echo $scout_i; ?></td></tr>
							<tr style="color: red;"><td title="Pênalti Perdido">PP (-3,5)</td><td><?php echo $scout_pp; ?></td></tr>
						</tbody>
					</table>
				</div>
			</div>
			<?php
		}
	}
 }

?>