<?php
include 'inc/funcoes.php';
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row">
				<h5>Testes</h5>
				<div id="testes" class="col s12 m6 l6 center">
					
					
					<?php
					
					$url_rodadas = "https://api.cartolafc.globo.com/rodadas";
					$json_rodadas = exec("curl -X GET ".$url_rodadas);
					$array_rodadas = json_decode($json_rodadas);
					
					foreach ($array_rodadas as $rodada) {
						if ($rodada->rodada_id == $rodada_atual) {
							$fimrodada = strtotime($rodada->fim);
						}
					}
					
					$agora = time();
					$posrodada = date("Y-m-d H:i:s", strtotime($fimrodada . " +5 hours"));
					//if (($agora-(60*60*24)) < $fimrodada) {
					if ($fimrodada < $agora && $agora < $posrodada) {
						echo 'data futura';
					} else {
						echo 'data passada';
					}
					
					
					//echo $posrodada;
					
					?>
					
					
					
					<?php 
					/* NÃO APAGAR!!!!!!!!!!!
					<script>
					var categories = ['FS', 'PE', 'A', 'FT', 'FD', 'FF', 'G', 'I', 'PP'], count = 0;
					Highcharts.chart('grafico_ataque', {
						chart: {polar: true},
						title: {text: 'Ataque'},
						colors: ['#317495'],

						xAxis: {
							tickInterval: 1,
							min: 0,
							max: 9,
							labels: {
								formatter: function () {
									var value = categories[count];

									count++;
									if (count == 10) {
											count = 0;
									}

									return value;
								}
							},
						},
						yAxis: {
							min: 0,
							//tickInterval: 5,
							//tickPositions: [0, 5, 10, 15],
							minorTickInterval: 0
						},
						
						tooltip: {
							formatter: function () {
								return categories[Highcharts.numberFormat(this.x, 0)] + ': <strong>' + this.y + '</strong>';
							}
						},

						plotOptions: {
							series: {
								pointStart: 0,
								pointInterval: 1
							},
							column: {
								pointPadding: 0,
								groupPadding: 0
							}
						},

						series: [{
							type: 'line',
							name: 'Scout',
							data: [2, 4, 5, 2, 6, 7, 4, 8, 9]
						}]
					});
					</script>
					<div id="grafico_ataque" style="height: 320px; margin: 0 auto"></div>
					*/
					?>
					
				</div>
			</div>
		</div>
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>