<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div class="row center">
				<h5>Sucesso</h5>			
				<p>O e-mail foi enviado com sucesso. Responderei sua mensagem em breve. Obrigado!</p>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>