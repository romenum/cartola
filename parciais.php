<?php
include 'inc/variaveis.php';

include 'inc/json_mercado_status.php';
include 'inc/json_atletas_mercado.php';
//include 'inc/json_atletas_pontuados.php';
include 'inc/json_clubes.php';

$url_atletas_pontuados = "https://api.cartolafc.globo.com/atletas/pontuados";
$json_atletas_pontuados = exec("curl -X GET ".$url_atletas_pontuados);
$array_atletas_pontuados = json_decode($json_atletas_pontuados);
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include 'inc/head.php'; ?>
	</head>

	<body class="teal lighten-5">
		<?php include 'inc/scriptsstart.php'; ?>
		<?php include 'inc/header.php'; ?>
		
		<div id="principal">
			<div id="tabela_parciais_container" class="row">
				<table id="tabela_parciais" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th class="th_input">Jogador</th>
							<th class="th_input">Time</th>
							<th class="th_input">Posição</th>
							<th class="th_input">Pontuação</th>
							<th class="th_input">Scouts</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Jogador</th>
							<th>Time</th>
							<th>Posição</th>
							<th>Pontuação</th>
							<th>Scouts</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
						
						if ($status_mercado == 1) {
							echo '<div class="center">
								<p id="art_mercado_fechado" class="card center yellow lighten-2 red-text text-darken-2">Aguarde a próxima 
								rodada iniciar para consultar as informações em tempo real.</p>
							</div>';
						} else {
							$ext_counter = 1;
							foreach ($array_atletas_pontuados->atletas as $atletas_key => $atletas_value) {
								$counter = 1;
								foreach ($atletas_value as $atletas_value_k => $atletas_value_v) {
									$atletas_pontuados_int["id"] =  $atletas_key;
									if ($counter == 1) {
										$atletas_pontuados_int["nome"] =  $atletas_value_v;
									} else if ($counter == 6) {
										$atletas_pontuados_int["clube"] =  $atletas_value_v;
									} else if ($counter == 5) {
										$atletas_pontuados_int["posicao"] =  $atletas_value_v;
									} else if ($counter == 2) {
										$atletas_pontuados_int["pontuacao"] =  $atletas_value_v;
									}
									$counter++;
								}
								$ext_counter++;
								$atletas_pontuados_ext[] = $atletas_pontuados_int;
							}
							
							foreach ($atletas_pontuados_ext as $atletas_pontuados) {
								echo '<tr>';
									echo '<td>' . $atletas_pontuados["nome"] . '</td>';
									
									foreach ($array_clubes as $clubes) {
										if ($clubes->id == $atletas_pontuados["clube"]) {
											$arr_clube = array (
												'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
											);
										}
									}
									echo '<td>' . $arr_clube["nome_clube"] . '</td>';
									
									echo '<td>' . ${'posicao' . $atletas_pontuados["posicao"]} . '</td>';
									
									if ($atletas_pontuados["pontuacao"] == 0) {
										$style_color_pontu = "";
									} else if ($atletas_pontuados["pontuacao"] < 0) {
										$style_color_pontu = ' style="color: red"';
									} else if ($atletas_pontuados["pontuacao"] > 0) {
										$style_color_pontu = ' style="color: green"';
									}
									echo '<td' . $style_color_pontu . '>' . $atletas_pontuados["pontuacao"] . '</td>';
									echo '<td>
										<a href="#modal_scouts" data-toggle="modal" data-id="' . $atletas_pontuados["id"] . '">VER</a>
									</td>';
								echo '</tr>';
							}
						}
						?>
					</tbody>
				</table>
			</div>
			<div id="modal_scouts" class="modal modal-fixed-footer">
				<div class="modal-header">
					<h5 class="left">Scouts do jogador</h5>
				</div>
				<div class="modal-content">
					<div class="fetched-data-scouts"></div>
				</div>
					<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn btn-flat">Fechar</a>
				</div>
			</div>
		</div>
		
		<?php include 'inc/scriptsend.php'; ?>
	</body>
</html>