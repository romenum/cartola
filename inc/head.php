<title>Escalação Automática para Cartola FC</title>
<meta name="author" content="Pedro Ferreira (Romenum)">
<meta name="description" content="Sistema de Escalação Automática para CartolaFC com I.A. (Inteligência Artificial)">
<meta name="keywords" content="Cartola, CartolaFC, Cartola FC, fantasy game, jogo, jogo fantasia, fantasy sports, esporte fantasia, 
esportes, futebol, campeonato brasileiro, campeonato, brasileirão, tabela brasileirão, brasileiro série A, brasileirão série A, 
escalação, times, escalar times, apostas, torcedores, Globo Esposte, pontuação, scouts, rodadas, mercado, parciais">
<meta charset="UTF-8">
<meta name=”robots” content=”follow”>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<meta property="og:title" content="Escalação Automática para CartolaFC">
<meta property="og:site_name" content="Escalação Automática - CartolaFC">
<meta property="og:description" content="Sistema de Escalação Automática para CartolaFC com I.A. (Inteligência Artificial)">
<meta property="og:locale" content="pt_BR">
<meta property="og:url" content="http://romenum.com/cartola">
<meta property="og:image" content="http://romenum.com/cartola/logo.jpg">
<meta property="og:image:type" content="image/jpeg">
<meta property="og:image:width" content="1600">
<meta property="og:image:height" content="1200">

<link type="text/css" rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" media="screen,projection"/>

<!--<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" media="screen,projection"/>-->
<!--<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" media="screen,projection"/>-->

<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="css/style.css"  media="screen,projection"/>
<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />

<!--<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.css"  media="screen,projection"/>-->
<!--<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"  media="screen,projection"/>-->
