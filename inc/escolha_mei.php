<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_meiocampistas = new stdClass;
$obj_meiocampistas->atleta_id = $atletas_mercado->atleta_id;
$obj_meiocampistas->atleta_apelido = $atletas_mercado->apelido;
$obj_meiocampistas->atleta_foto_140 = $foto_atleta_140;
$obj_meiocampistas->atleta_foto_80 = $foto_atleta_80;
$obj_meiocampistas->atleta_clube = $atletas_mercado->clube_id;
$obj_meiocampistas->status_id = $atletas_mercado->status_id;
$obj_meiocampistas->pontos_num = $atletas_mercado->pontos_num;
$obj_meiocampistas->preco_num = $atletas_mercado->preco_num;
$obj_meiocampistas->variacao_num = $atletas_mercado->variacao_num;
$obj_meiocampistas->media_num = $atletas_mercado->media_num;
$obj_meiocampistas->jogos_num = $atletas_mercado->jogos_num;
if (!empty($atletas_mercado->scout->RB)) {$obj_meiocampistas->scout_roubabola = $atletas_mercado->scout->RB;} else {$obj_meiocampistas->scout_roubabola = 0;}
if (!empty($atletas_mercado->scout->A)) {$obj_meiocampistas->scout_assistencia = $atletas_mercado->scout->A;} else {$obj_meiocampistas->scout_assistencia = 0;}
if (!empty($atletas_mercado->scout->G)) {$obj_meiocampistas->scout_gol = $atletas_mercado->scout->G;} else {$obj_meiocampistas->scout_gol = 0;}
if (!empty($atletas_mercado->scout->FF)) {$obj_meiocampistas->scout_ff = $atletas_mercado->scout->FF;} else {$obj_meiocampistas->scout_ff = 0;}
if (!empty($atletas_mercado->scout->FD)) {$obj_meiocampistas->scout_fd = $atletas_mercado->scout->FD;} else {$obj_meiocampistas->scout_fd = 0;}

if ($objetivo == 1) { // VALORIZAÇÃO
	if($obj_meiocampistas->jogos_num == 0 && $obj_meiocampistas->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$meiocampista_valorizar[] = $obj_meiocampistas;
	} else if ($obj_meiocampistas->jogos_num == 1 && $obj_meiocampistas->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$meiocampista_valorizar[] = $obj_meiocampistas;
	} else if ($obj_meiocampistas->jogos_num == 2 && ($obj_meiocampistas->pontos_num >= 0 && $obj_meiocampistas->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$meiocampista_valorizar[] = $obj_meiocampistas;
	} else if ($obj_meiocampistas->jogos_num > 2 && $obj_meiocampistas->media_num >= 2 && 
	($obj_meiocampistas->pontos_num >= -3 && $obj_meiocampistas->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$meiocampista_valorizar[] = $obj_meiocampistas;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_meiocampistas->jogos_num == 0 && ($obj_meiocampistas->preco_num >= 5 && $obj_meiocampistas->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$meiocampista_pontuar[] = $obj_meiocampistas;
	} else if ($obj_meiocampistas->jogos_num == 1 && ($obj_meiocampistas->preco_num >= 7 && $obj_meiocampistas->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$meiocampista_pontuar[] = $obj_meiocampistas;
	} else if ($obj_meiocampistas->jogos_num == 2 && ($obj_meiocampistas->scout_roubabola/$obj_meiocampistas->jogos_num >= 2 || 
	$obj_meiocampistas->scout_assistencia/$obj_meiocampistas->jogos_num >= 0.3 || $obj_meiocampistas->scout_gol/$obj_meiocampistas->jogos_num >= 0.3 || 
	$obj_meiocampistas->scout_fd/$obj_meiocampistas->jogos_num >= 0.6 || 
	$obj_meiocampistas->scout_ff/$obj_meiocampistas->jogos_num >= 0.8)) { // RODADA 3 DO JOGADOR
		$meiocampista_pontuar[] = $obj_meiocampistas;
	} else if (($obj_meiocampistas->jogos_num > 2 && $obj_meiocampistas->media_num >= 3) && 
	($obj_meiocampistas->scout_roubabola/$obj_meiocampistas->jogos_num >= 2 || 	$obj_meiocampistas->scout_assistencia/$obj_meiocampistas->jogos_num >= 0.3 || 
	$obj_meiocampistas->scout_gol/$obj_meiocampistas->jogos_num >= 0.3 || $obj_meiocampistas->scout_fd/$obj_meiocampistas->jogos_num >= 0.6 || 
	$obj_meiocampistas->scout_ff/$obj_meiocampistas->jogos_num >= 0.8)) { // RODADA 4 DO JOGADOR EM DIANTE
		$meiocampista_pontuar[] = $obj_meiocampistas;
	}
}

?>