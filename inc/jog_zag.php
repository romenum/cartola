<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 3 && $atletas_mercado->preco_num <= $peso_jog_def*$investimento ) {
		include 'inc/escolha_zag.php';
	}
}


if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($zagueiro_valorizar) && count($zagueiro_valorizar) >= ${'f' . $tipoformacao . '_qtde_zag'}) {
		if ($perfil == 1) {
			usort($zagueiro_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($zagueiro_valorizar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_zag'} . '_zagueiros'} = array_slice($zagueiro_valorizar, 0, ${'f' . $tipoformacao . '_qtde_zag'}, true);
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($zagueiro_pontuar) && count($zagueiro_pontuar) >= ${'f' . $tipoformacao . '_qtde_zag'}) {
		if ($perfil == 1) {
			usort($zagueiro_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($zagueiro_pontuar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_zag'} . '_zagueiros'} = array_slice($zagueiro_pontuar, 0, ${'f' . $tipoformacao . '_qtde_zag'}, true);
	}
}

if (($objetivo == 1 && !empty($zagueiro_valorizar) && count($zagueiro_valorizar) >= ${'f' . $tipoformacao . '_qtde_zag'}) || 
($objetivo == 2 && !empty($zagueiro_pontuar) && count($zagueiro_pontuar) >= ${'f' . $tipoformacao . '_qtde_zag'})) {
	for ($i=0; $i<${'f' . $tipoformacao . '_qtde_zag'}; $i++) {
		// CLUBE DO ATLETA
		$clube_atleta = ${'qtde' . ${'f' . $tipoformacao . '_qtde_zag'} . '_zagueiros'}[$i]->atleta_clube;
		foreach ($array_clubes as $clubes) {
			if ((!empty($zagueiro_valorizar) && $clubes->id == $zagueiro_valorizar[$i]->atleta_clube) || 
			(!empty($zagueiro_pontuar) && $clubes->id == $zagueiro_pontuar[$i]->atleta_clube)) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_zag[] = $arr_clube;
			}
		}
	}
}

?>