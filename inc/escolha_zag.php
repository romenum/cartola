<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_zagueiros = new stdClass;
$obj_zagueiros->atleta_id = $atletas_mercado->atleta_id;
$obj_zagueiros->atleta_apelido = $atletas_mercado->apelido;
$obj_zagueiros->atleta_foto_140 = $foto_atleta_140;
$obj_zagueiros->atleta_foto_80 = $foto_atleta_80;
$obj_zagueiros->atleta_clube = $atletas_mercado->clube_id;
$obj_zagueiros->status_id = $atletas_mercado->status_id;
$obj_zagueiros->pontos_num = $atletas_mercado->pontos_num;
$obj_zagueiros->preco_num = $atletas_mercado->preco_num;
$obj_zagueiros->variacao_num = $atletas_mercado->variacao_num;
$obj_zagueiros->media_num = $atletas_mercado->media_num;
$obj_zagueiros->jogos_num = $atletas_mercado->jogos_num;
if (!empty($atletas_mercado->scout->RB)) {$obj_zagueiros->scout_roubabola = $atletas_mercado->scout->RB;} else {$obj_zagueiros->scout_roubabola = 0;}
if (!empty($atletas_mercado->scout->SG)) {$obj_zagueiros->scout_saldo = $atletas_mercado->scout->SG;} else {$obj_zagueiros->scout_saldo = 0;}
if (!empty($atletas_mercado->scout->G)) {$obj_zagueiros->scout_gol = $atletas_mercado->scout->G;} else {$obj_zagueiros->scout_gol = 0;}
if (!empty($atletas_mercado->scout->A)) {$obj_zagueiros->scout_assistencia = $atletas_mercado->scout->A;} else {$obj_zagueiros->scout_assistencia = 0;}

if ($objetivo == 1) { // VALORIZAÇÃO
	if($obj_zagueiros->jogos_num == 0 && $obj_zagueiros->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$zagueiro_valorizar[] = $obj_zagueiros;
	} else if ($obj_zagueiros->jogos_num == 1 && $obj_zagueiros->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$zagueiro_valorizar[] = $obj_zagueiros;
	} else if ($obj_zagueiros->jogos_num == 2 && ($obj_zagueiros->pontos_num >= 0 && $obj_zagueiros->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$zagueiro_valorizar[] = $obj_zagueiros;
	} else if ($obj_zagueiros->jogos_num > 2 && $obj_zagueiros->media_num > 2 && 
	($obj_zagueiros->pontos_num >= -3 && $obj_zagueiros->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$zagueiro_valorizar[] = $obj_zagueiros;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_zagueiros->jogos_num == 0 && ($obj_zagueiros->preco_num >= 5 && $obj_zagueiros->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$zagueiro_pontuar[] = $obj_zagueiros;
	} else if ($obj_zagueiros->jogos_num == 1 && ($obj_zagueiros->preco_num >= 7 && $obj_zagueiros->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$zagueiro_pontuar[] = $obj_zagueiros;
	} else if ($obj_zagueiros->jogos_num == 2 && ($obj_zagueiros->scout_saldo/$obj_zagueiros->jogos_num >= 0.3 || 
	$obj_zagueiros->scout_roubabola/$obj_zagueiros->jogos_num >= 2 || $obj_zagueiros->scout_assistencia/$obj_zagueiros->jogos_num >= 0.3 || 
	$obj_zagueiros->scout_gol/$obj_zagueiros->jogos_num >= 0.2)) { // RODADA 3 DO JOGADOR
		$zagueiro_pontuar[] = $obj_zagueiros;
	} else if (($obj_zagueiros->jogos_num > 2 && $obj_zagueiros->media_num >= 3) && ($obj_zagueiros->scout_saldo/$obj_zagueiros->jogos_num >= 0.3 || 
	$obj_zagueiros->scout_roubabola/$obj_zagueiros->jogos_num >= 2 || $obj_zagueiros->scout_assistencia/$obj_zagueiros->jogos_num >= 0.3 || 
	$obj_zagueiros->scout_gol/$obj_zagueiros->jogos_num >= 0.2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$zagueiro_pontuar[] = $obj_zagueiros;
	}
}

?>