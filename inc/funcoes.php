<?php

function sortByMedia($a, $b) {
    return strnatcasecmp($b->media_num, $a->media_num);
}

function sortByPreco($a, $b) {
    return strnatcasecmp($b->preco_num, $a->preco_num);
}

function sortByVariacao($a, $b) {
    return strnatcasecmp($b->variacao_num, $a->variacao_num);
}

function sortByGol($a, $b) {
    return strnatcasecmp($b->scout_gol, $a->scout_gol);
}

function csvToJson($fname) {
	// open csv file
	if (!($fp = fopen($fname, 'r'))) {
		die("Can't open file...");
	}
	
	//read csv headers
	$key = fgetcsv($fp,"1024",",");
	
	// parse csv rows into array
	$json = array();
		while ($row = fgetcsv($fp,"1024",",")) {
		$json[] = array_combine($key, $row);
	}
	
	// release file handle
	fclose($fp);
	
	// encode array to json
	return json_encode($json);
}

?>