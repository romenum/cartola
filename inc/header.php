<header>
	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper teal green-text text-lighten-5">
				<a id="logo_cartola" href="index.php" class="brand-logo right">
					<img title="Cartola FC" alt="Cartola FC" src="img/logo_cartola.png" />
				</a>
				<a href="#" data-activates="mobile-demo" class="button-collapse left"><i class="material-icons">menu</i></a>
				<ul class="left hide-on-med-and-down">
					<?php include 'menu.php'; ?>
				</ul>
			</div>
		</nav>
	</div>
	<div id="mobile-demo" class="side-nav teal green-text text-lighten-5">
		<ul>
			<?php include 'menu.php'; ?>
		</ul>
		<div id="creditos">
			<p>Sistema em desenvolvimento<br />
			Por: <strong>Pedro Ferreira (Romenum)</strong></p>
		</div>
	</div>
</header>