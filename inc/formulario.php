
<script>
$(document).ready(function () {	
	$('#reserva_patrimonio, #patrimonio_atual').on('change keyup', function(){
		var patrimonio_atual = $("#patrimonio_atual").val();
		var reserva_patrimonio = $("#reserva_patrimonio").val();
		var restante = (reserva_patrimonio / 100) * patrimonio_atual;
		var invest = patrimonio_atual - restante;
		$(".invest_patrimonio").text(parseFloat(Math.round(invest * 100) / 100).toFixed(2));
		$(".result_patrimonio").text(parseFloat(Math.round(restante * 100) / 100).toFixed(2));
	});
	
	var formacaoEscolhida = 'formacao0';
	$('#tipo_formacao').change(function(){
		$('div#formacao_campo').removeClass(formacaoEscolhida).addClass($(this).val());
		formacaoEscolhida = $(this).val();
	});
	
	$('#invest_areacampo').on('load change', function() {
		if($(this).val() == ''){
			$('#invest_defesa').removeClass();
			$("#invest_meiocampo").removeClass();
			$("#invest_ataque").removeClass();
		} else if($(this).val() == 1){
			$('#invest_defesa').addClass('maisdinheiro');
			$("#invest_meiocampo").removeClass('maisdinheiro');
			$("#invest_ataque").removeClass('maisdinheiro');
		} else if($(this).val() == 2){
			$('#invest_defesa').removeClass('maisdinheiro');
			$('#invest_meiocampo').addClass('maisdinheiro');
			$("#invest_ataque").removeClass('maisdinheiro');
		} else if($(this).val() == 3){
			$('#invest_defesa').removeClass('maisdinheiro');
			$("#invest_meiocampo").removeClass('maisdinheiro');
			$('#invest_ataque').addClass('maisdinheiro');
		} else if($(this).val() == 4){
			$('#invest_defesa').removeClass(); $('#invest_defesa').addClass('maisdinheiro');
			$("#invest_meiocampo").removeClass(); $('#invest_meiocampo').addClass('maisdinheiro');
			$("#invest_ataque").removeClass(); $('#invest_ataque').addClass('maisdinheiro');
		}
	});
		
});
</script>

<h5><?php echo $var_preencher_formulario; ?></h5>
<form id="sugerirformacao" action="" method="POST">
	<div class="row">
		<div class="input-field col s6">
			<input placeholder="Exemplo: 120.56" id="patrimonio_atual" name="patrimonio-atual" type="text" 
			<?php if (isset($_POST["sugerirformacao"])) {echo 'value="'.$patrimonioatual.'"';} ?>
			class="validate" required="" aria-required="true" />
			<label for="patrimonio_atual"><?php echo $var_meu_patrimonio_atual; ?></label>
		</div>

		<div class="input-field col s6">
			<select id="reserva_patrimonio" name="reserva-patrimonio" class="validate" required="" aria-required="true">
				<?php if (!isset($_POST["sugerirformacao"])) { echo '<option value="" disabled selected>' . $var_escolher_opcao . '</option>'; } ?>
				<?php
					for ($i=0; $i<11; $i++) {
						echo '<option value="' . $i*5 . '"';
						if (isset($_POST["sugerirformacao"])) {if ($reservapatrimonio == $i*5) {echo ' selected selected="selected"';}}
						echo '>' . $i*5 . '%</option>';
					}
				?>
			</select>
			<label><?php echo $var_quero_economizar; ?></label>
		</div>
	</div>
	
	<div id="patrimonio_restante" class="row">
		<div class="input-field col s12 amber lighten-3 orange-text text-darken-4">
			<div><?php echo $var_posso_investir; ?> 
				<strong>C$ 
					<span id="invest_patrimonio" class="invest_patrimonio">
						<?php if (isset($_POST["sugerirformacao"])) {echo $investimento;} ?>
					</span>
				</strong>
			</div>
		</div>
		<div class="input-field col s12 deep-purple lighten-4 purple-text text-darken-2">
			<div><?php echo $var_quero_economizar; ?> 
				<strong>C$ 
					<span id="result_patrimonio" class="result_patrimonio">
						<?php if (isset($_POST["sugerirformacao"])) {echo $patrimonio_economizado;} ?>
					</span>
				</strong>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col s6">
			<select id="tipo_formacao" name="tipo_formacao" class="validate" required="" aria-required="true">
				<?php if (!isset($_POST["sugerirformacao"])) { echo '<option value="" disabled selected>' . $var_escolher_opcao . '</option>'; } ?>
				<?php
					for ($i=1; $i<8; $i++) {
						echo '<option value="formacao' . $i . '"';
						if (isset($_POST["sugerirformacao"])) {if ($tipoformacao == $i) {echo ' selected selected="selected"';}}
						echo '>' . ${'formacao' . $i} . '</option>';
					}
				?>
			</select>
			<label><?php echo $var_formacao_desejada; ?></label>
		</div>

		<div class="input-field col s6">
			<select id="invest_areacampo" name="invest_areacampo" class="validate" required="" aria-required="true">
				<?php if (!isset($_POST["sugerirformacao"])) { echo '<option value="" disabled selected>' . $var_escolher_opcao . '</option>'; } ?>
				<option value="1"<?php if (isset($_POST["sugerirformacao"])) {if ($invest_areacampo == 1) {
				echo ' selected selected="selected"';}} ?>><?php echo $var_area_defesa; ?></option>
				<option value="2"<?php if (isset($_POST["sugerirformacao"])) {if ($invest_areacampo == 2) {
				echo ' selected selected="selected"';}} ?>><?php echo $var_area_meiocampo; ?></option>
				<option value="3"<?php if (isset($_POST["sugerirformacao"])) {if ($invest_areacampo == 3) {
				echo ' selected selected="selected"';}} ?>><?php echo $var_area_ataque; ?></option>
				<option value="4"<?php if (isset($_POST["sugerirformacao"])) {if ($invest_areacampo == 4) {
				echo ' selected selected="selected"';}} ?>><?php echo $var_area_igual; ?></option>
			</select>
			<label><?php echo $var_area_maior_invest; ?></label>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col s6">
			<select id="perfil" name="perfil" class="validate" required="" aria-required="true">
				<?php if (!isset($_POST["sugerirformacao"])) { echo '<option value="" disabled selected>' . $var_escolher_opcao . '</option>'; } ?>
				<option value="1"<?php if (isset($_POST["sugerirformacao"])) {
				if ($perfil == 1) {echo ' selected selected="selected"';}} ?>><?php echo $var_perfil_conservador; ?></option>
				<?php /*
				<option value="2"<?php if (isset($_POST["sugerirformacao"])) {
				if ($perfil == 2) {echo ' selected selected="selected"';}} ?>><?php echo $var_perfil_equilibrado; ?></option>
				*/ ?>
				<option value="2"<?php if (isset($_POST["sugerirformacao"])) {
				if ($perfil == 2) {echo ' selected selected="selected"';}} ?>><?php echo $var_perfil_apostador; ?></option>
			</select>
			<label><?php echo $var_meu_perfil; ?></label>
		</div>

		<div class="input-field col s6">
			<select id="objetivo" name="objetivo" class="validate" required="" aria-required="true">
				<?php if (!isset($_POST["sugerirformacao"])) { echo '<option value="" disabled selected>' . $var_escolher_opcao . '</option>'; } ?>
				<option value="1"<?php if (isset($_POST["sugerirformacao"])) {
				if ($objetivo == 1) {echo ' selected selected="selected"';}} ?>><?php echo $var_objetivo_valorizacao; ?></option>
				<option value="2"<?php if (isset($_POST["sugerirformacao"])) {
				if ($objetivo == 2) {echo ' selected selected="selected"';}} ?>><?php echo $var_objetivo_pontuacao; ?></option>
			</select>
			<label><?php echo $var_meu_objetivo; ?></label>
		</div>
	</div>
	
	<div class="row center">
		<button id="sugerirformacao" class="btn waves-light cyan darken-3" 
		type="submit" name="sugerirformacao"<?php if($status_mercado != 1) {echo ' disabled disabled="disabled"';} ?>><?php echo $var_obter_escalacao; ?>
			<i class="material-icons right">send</i>
		</button>
		<?php if($status_mercado != 1) {
			echo '<p id="msg_mercado_fechado" class="card center yellow lighten-2 red-text text-darken-2">O mercado está fechado!</p>';
		} else {
			echo '<p id="msg_mercado_fechado" class="card center yellow lighten-2 red-text text-darken-2">O mercado fecha em: <span id="timer"></span></p>';
		} ?>
	</div>
</form>
