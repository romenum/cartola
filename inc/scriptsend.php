<!-- Script Materialize -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
<script>
$(".button-collapse").sideNav({ menuWidth: 280 });
$(".novaescalacao").sideNav({ menuWidth: 320 });
$('select').material_select();
$('.modal').modal();
$('ul.tabs').tabs();
$('.collapsible').collapsible();
$('.modal-trigger').leanModal({
	ready: function () {
		$('ul.tabs').tabs();
	}
});


//$('.carousel').carousel();
//$('.carousel.carousel-slider').carousel({fullWidth: true});
</script>

<!-- Script Countdown -->
<script>
$(document).ready(function () {
	$('#timer').countdown({
		until: new Date(<?php echo $fechamento_ano; ?>, <?php echo $fechamento_mes; ?>-1, 
		<?php echo $fechamento_dia; ?>, <?php echo $fechamento_hora; ?>, <?php echo $fechamento_minuto; ?>),
		compact: true
	});
});
</script>

<!-- Script DataTables - Parciais -->
<script>
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#tabela_parciais tfoot th').each( function () {
        var title = $(this).text();
		if (title === "Jogador" || title === "Time" || title === "Posição") {
			$(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
		} else {
			$(this).html( '' );
		}
        
    } );
 
    // DataTable
    var table = $('#tabela_parciais').DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		}
	});
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>

<!-- Script DataTables - Raio-X -->
<script>
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#tabela_raiox tfoot th').each( function () {
        var title = $(this).text();
		if (title === "Jogador" || title === "Time" || title === "Posição") {
			$(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
		} else {
			$(this).html( '' );
		}
        
    } );
 
    // DataTable
    var table = $('#tabela_raiox').DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		}
	});
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>

<!-- Script Mais Infos do Jogador (Escalação - Mapa) -->
<script>
$(document).ready(function(){
	$('#modal_infojogador').on('show.bs.modal', function (e) {
		$('.fetched-data').html('<p>Carregando dados...</p>');
		var jogadorid = $(e.relatedTarget).data('id');
		$.ajax({
			type : 'post',
			url : 'infojogador.php', //Here you will fetch records 
			data :  'jogadorid='+ jogadorid, //Pass $id
			success : function(data){
				$('.fetched-data').html(data);//Show fetched data from database
			}
		});
	});
});
</script>

<!-- Script Raio-X do Jogador -->
<script>
$(document).ready(function(){
	$('#modal_raiox').on('show.bs.modal', function (e) {
		$('.fetched-data-raiox').html('<p>Carregando dados...</p>');
		var jogadorid = $(e.relatedTarget).data('id');
		$.ajax({
			type : 'post',
			url : 'raioxjogador.php', //Here you will fetch records 
			data :  'jogadorid='+ jogadorid, //Pass $id
			success : function(data){
				$('.fetched-data-raiox').html(data);//Show fetched data from database
				$('#firstTab').click();
				$('ul.tabs').tabs();
			}
		});
	});
});
</script>

<!-- Script Scouts do Jogador (Parciais) -->
<script>
$(document).ready(function(){
	$('#modal_scouts').on('show.bs.modal', function (e) {
		$('.fetched-data-scouts').html('<p>Carregando dados...</p>');
		var jogadorid = $(e.relatedTarget).data('id');
		$.ajax({
			type : 'post',
			url : 'scoutsjogador.php', //Here you will fetch records 
			data :  'jogadorid='+ jogadorid, //Pass $id
			success : function(data){
				$('.fetched-data-scouts').html(data);//Show fetched data from database
			}
		});
	 });
});
</script>