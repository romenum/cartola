<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_atacantes = new stdClass;
$obj_atacantes->atleta_id = $atletas_mercado->atleta_id;
$obj_atacantes->atleta_apelido = $atletas_mercado->apelido;
$obj_atacantes->atleta_foto_140 = $foto_atleta_140;
$obj_atacantes->atleta_foto_80 = $foto_atleta_80;
$obj_atacantes->atleta_clube = $atletas_mercado->clube_id;
$obj_atacantes->status_id = $atletas_mercado->status_id;
$obj_atacantes->pontos_num = $atletas_mercado->pontos_num;
$obj_atacantes->preco_num = $atletas_mercado->preco_num;
$obj_atacantes->variacao_num = $atletas_mercado->variacao_num;
$obj_atacantes->media_num = $atletas_mercado->media_num;
$obj_atacantes->jogos_num = $atletas_mercado->jogos_num;
if (!empty($atletas_mercado->scout->G)) {$obj_atacantes->scout_gol = $atletas_mercado->scout->G;} else {$obj_atacantes->scout_gol = 0;}
if (!empty($atletas_mercado->scout->A)) {$obj_atacantes->scout_a = $atletas_mercado->scout->A;} else {$obj_atacantes->scout_a = 0;}
if (!empty($atletas_mercado->scout->FT)) {$obj_atacantes->scout_ft = $atletas_mercado->scout->FT;} else {$obj_atacantes->scout_ft = 0;}
if (!empty($atletas_mercado->scout->FD)) {$obj_atacantes->scout_fd = $atletas_mercado->scout->FD;} else {$obj_atacantes->scout_fd = 0;}
if (!empty($atletas_mercado->scout->FF)) {$obj_atacantes->scout_ff = $atletas_mercado->scout->FF;} else {$obj_atacantes->scout_ff = 0;}

if ($objetivo == 1) { // VALORIZAÇÃO
	if ($obj_atacantes->jogos_num == 0 && $obj_atacantes->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$atacante_valorizar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num == 1 && $obj_atacantes->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$atacante_valorizar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num == 2 && ($obj_atacantes->pontos_num >= 0 && $obj_atacantes->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$atacante_valorizar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num > 2 && $obj_atacantes->media_num >= 2 && 
	($obj_atacantes->pontos_num >= -3 && $obj_atacantes->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$atacante_valorizar[] = $obj_atacantes;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_atacantes->jogos_num == 0 && ($obj_atacantes->preco_num >= 5 && $obj_atacantes->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$atacante_pontuar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num == 1 && ($obj_atacantes->preco_num >= 7 && $obj_atacantes->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$atacante_pontuar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num == 2 && ($obj_atacantes->scout_gol/$obj_atacantes->jogos_num >= 0.3 || 
	$obj_atacantes->scout_fd/$obj_atacantes->jogos_num >= (0.15*$obj_atacantes->jogos_num) || 
	$obj_atacantes->scout_a/$obj_atacantes->jogos_num >= (0.12*$obj_atacantes->jogos_num))) { // RODADA 3 DO JOGADOR
		$atacante_pontuar[] = $obj_atacantes;
	} else if ($obj_atacantes->jogos_num > 2 && $obj_atacantes->media_num >= 5 && 
	($obj_atacantes->scout_gol/$obj_atacantes->jogos_num >= 0.3 || $obj_atacantes->scout_fd/$obj_atacantes->jogos_num >= 0.5 || 
	$obj_atacantes->scout_a/$obj_atacantes->jogos_num >= 0.3)) { // RODADA 4 DO JOGADOR EM DIANTE
		$atacante_pontuar[] = $obj_atacantes;
	}
}

?>