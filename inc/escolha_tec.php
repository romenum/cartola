<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_tecnico = new stdClass;
$obj_tecnico->atleta_id = $atletas_mercado->atleta_id;
$obj_tecnico->atleta_apelido = $atletas_mercado->apelido;
$obj_tecnico->atleta_foto_140 = $foto_atleta_140;
$obj_tecnico->atleta_foto_80 = $foto_atleta_80;
$obj_tecnico->atleta_clube = $atletas_mercado->clube_id;
$obj_tecnico->status_id = $atletas_mercado->status_id;
$obj_tecnico->pontos_num = $atletas_mercado->pontos_num;
$obj_tecnico->preco_num = $atletas_mercado->preco_num;
$obj_tecnico->variacao_num = $atletas_mercado->variacao_num;
$obj_tecnico->media_num = $atletas_mercado->media_num;
$obj_tecnico->jogos_num = $atletas_mercado->jogos_num;

if ($objetivo == 1) { // VALORIZAÇÃO
	if($obj_tecnico->jogos_num == 0 && $obj_tecnico->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$tecnico_valorizar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num == 1 && $obj_tecnico->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$tecnico_valorizar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num == 2 && 
	($obj_tecnico->pontos_num >= 0 && $obj_tecnico->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$tecnico_valorizar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num > 2 && $obj_tecnico->media_num >= 2 && 
	($obj_tecnico->pontos_num >= -3 && $obj_tecnico->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$tecnico_valorizar[] = $obj_tecnico;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_tecnico->jogos_num == 0 && ($obj_tecnico->preco_num >= 5 && $obj_tecnico->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$tecnico_pontuar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num == 1 && ($obj_tecnico->preco_num >= 7 && $obj_tecnico->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$tecnico_pontuar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num == 2 && $obj_tecnico->media_num >= 3) { // RODADA 3 DO JOGADOR
		$tecnico_pontuar[] = $obj_tecnico;
	} else if ($obj_tecnico->jogos_num > 2 && $obj_tecnico->media_num >= 2.5) { // RODADA 4 DO JOGADOR EM DIANTE
		$tecnico_pontuar[] = $obj_tecnico;
	}
}

?>