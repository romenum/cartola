<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 4 && $atletas_mercado->preco_num <= $peso_jog_mei*$investimento ) {
		include 'inc/escolha_mei.php';
	}
}

if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($meiocampista_valorizar) && count($meiocampista_valorizar) >= ${'f' . $tipoformacao . '_qtde_mei'}) {
		if ($perfil == 1) {
			usort($meiocampista_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($meiocampista_valorizar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_mei'} . '_meiocampistas'} = 
		array_slice($meiocampista_valorizar, 0, ${'f' . $tipoformacao . '_qtde_mei'}, true);
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($meiocampista_pontuar) && count($meiocampista_pontuar) >= ${'f' . $tipoformacao . '_qtde_mei'}) {
		if ($perfil == 1) {
			usort($meiocampista_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($meiocampista_pontuar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_mei'} . '_meiocampistas'} = 
		array_slice($meiocampista_pontuar, 0, ${'f' . $tipoformacao . '_qtde_mei'}, true);
	}
}

if (($objetivo == 1 && !empty($meiocampista_valorizar) && count($meiocampista_valorizar) >= ${'f' . $tipoformacao . '_qtde_mei'}) || 
($objetivo == 2 && !empty($meiocampista_pontuar) && count($meiocampista_pontuar) >= ${'f' . $tipoformacao . '_qtde_mei'})) {
	for ($i=0; $i < ${'f' . $tipoformacao . '_qtde_mei'}; $i++) {
		// CLUBE DO ATLETA
		$clube_atleta = ${'qtde' . ${'f' . $tipoformacao . '_qtde_mei'} . '_meiocampistas'}[$i]->atleta_clube;
		foreach ($array_clubes as $clubes) {
			if ((!empty($meiocampista_valorizar) && $clubes->id == $meiocampista_valorizar[$i]->atleta_clube) || 
			(!empty($meiocampista_pontuar) && $clubes->id == $meiocampista_pontuar[$i]->atleta_clube)) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_mei[] = $arr_clube;
			}
		}
	}
}

?>