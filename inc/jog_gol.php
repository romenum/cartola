<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 1 && $atletas_mercado->preco_num <= $peso_jog_def*$investimento ) {
		include 'inc/escolha_gol.php';
	}
}

$pointer_erro = 0;
if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($goleiro_valorizar)) {
		if ($perfil == 1) {
			usort($goleiro_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($goleiro_valorizar, "sortByPreco");
		}
		$qtde1_goleiros = array_slice($goleiro_valorizar, 0, 1, true);
	} else {
		$pointer_erro++;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($goleiro_pontuar)) {
		if ($perfil == 1) {
			usort($goleiro_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($goleiro_pontuar, "sortByPreco");
		}
		$qtde1_goleiros = array_slice($goleiro_pontuar, 0, 1, true);
	} else {
		$pointer_erro++;
	}
}

if ($pointer_erro == 0) {
	for ($i=0; $i<1; $i++) {
		// CLUBE DO ATLETA
		$clube_atleta = $qtde1_goleiros[$i]->atleta_clube;
		foreach ($array_clubes as $clubes) {
			if ($clubes->id == $clube_atleta) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_gol[] = $arr_clube;
			}
		}
	}
}

?>