<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 6 && $atletas_mercado->preco_num <= $peso_tecnico*$investimento ) {
		include 'inc/escolha_tec.php';
	}
}

$pointer_erro = 0;
if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($tecnico_valorizar)) {
		if ($perfil == 1) {
			usort($tecnico_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($tecnico_valorizar, "sortByPreco");
		}
		$qtde1_tecnicos = array_slice($tecnico_valorizar, 0, 1, true);
	} else {
		$pointer_erro ++;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($tecnico_pontuar)) {
		if ($perfil == 1) {
			usort($tecnico_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($tecnico_pontuar, "sortByPreco");
		}
		$qtde1_tecnicos = array_slice($tecnico_pontuar, 0, 1, true);
	} else {
		$pointer_erro++;
	}
}

if ($pointer_erro == 0) {
	for ($i=0; $i<1; $i++) {
		// CLUBE DO ATLETA
		$clube_atleta = $qtde1_tecnicos[$i]->atleta_clube;
		foreach ($array_clubes as $clubes) {
			if ($clubes->id == $clube_atleta) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_tec[] = $arr_clube;
			}
		}
	}
}

?>