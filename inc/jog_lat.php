<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 2 && $atletas_mercado->preco_num <= $peso_jog_def*$investimento ) {
		include 'inc/escolha_lat.php';
	}
}

if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($lateral_valorizar) && count($lateral_valorizar) >= ${'f' . $tipoformacao . '_qtde_lat'}) {
		if ($perfil == 1) {
			usort($lateral_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($lateral_valorizar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_lat'} . '_laterais'} = array_slice($lateral_valorizar, 0, ${'f' . $tipoformacao . '_qtde_lat'}, true);
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($lateral_pontuar) && count($lateral_pontuar) >= ${'f' . $tipoformacao . '_qtde_lat'}) {
		if ($perfil == 1) {
			usort($lateral_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($lateral_pontuar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_lat'} . '_laterais'} = array_slice($lateral_pontuar, 0, ${'f' . $tipoformacao . '_qtde_lat'}, true);
	}
}

if (($objetivo == 1 && !empty($lateral_valorizar) && count($lateral_valorizar) >= ${'f' . $tipoformacao . '_qtde_lat'}) || 
($objetivo == 2 && !empty($lateral_pontuar) && count($lateral_pontuar) >= ${'f' . $tipoformacao . '_qtde_lat'})) {
	for ($i=0; $i<${'f' . $tipoformacao . '_qtde_lat'}; $i++) {
		// CLUBE DO ATLETA
		$clube_atleta = ${'qtde' . ${'f' . $tipoformacao . '_qtde_lat'} . '_laterais'}[$i]->atleta_clube;
		foreach ($array_clubes as $clubes) {
			if ((!empty($lateral_valorizar) && $clubes->id == $lateral_valorizar[$i]->atleta_clube) || 
			(!empty($lateral_pontuar) && $clubes->id == $lateral_pontuar[$i]->atleta_clube)) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_lat[] = $arr_clube;
			}
		}
	}
}

?>