
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Pontuação</a></li>
	<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Cartoletas</a></li>
	<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Scouts</a></li>
</ul>

<div class="tab-content">
	<!--<div class="clearfix"></div>-->
	<div role="tabpanel" class="tab-pane active" id="tab1">
		<script>
		Highcharts.chart('grafico_pontuacao', {
			chart: {
				backgroundColor: 'transparent'
			},
			title: {text: 'Pontuação por rodada'},
			colors: ['#26ca5e', '#666666'],
			tooltip: {
				formatter: function() {
					return '<strong>' + parseFloat(Math.round(this.y * 100) / 100).toFixed(1) + ' pontos</strong>';
				},
				backgroundColor: '#e3672a',
				borderWidth: 0,
				style: {
					color: 'white'
				}
			},
			xAxis: {categories: [
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i != $qtde_rodadas) {
					echo "'" . $arr_rodadas[$i] . "', ";
				} else {
					echo "'" . $arr_rodadas[$i] . "'";
				}
			}
			?>
			]},
			yAxis: {title: {text: 'Pontuação'}},
			labels: {
				items: [{
					//html: 'Pontos e médias por rodada',
					style: {
						left: '50px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			},
			series: [{
				type: 'column',
				name: 'Pontos',
				data: [
				<?php
				for ($i=1; $i<=$qtde_rodadas; $i++) {
					if ($i != $qtde_rodadas) {
						echo $arr_pontuacao[$i] . ", ";
					} else {
						echo $arr_pontuacao[$i];
					}
				}
				?>
				],
				dataLabels: {
					enabled: true,
					//rotation: -90,
					color: '#06a33c',
					align: 'center',
					format: '{point.y:.1f}', // one decimal
					y: 0, // 10 pixels down from the top
					style: {
						fontSize: '12px',
						fontFamily: 'Arial, sans-serif'
					}
				}
			}, {
				type: 'spline',
				name: 'Média',
				data: [
				<?php
				for ($i=1; $i<=$qtde_rodadas; $i++) {
					if ($i != $qtde_rodadas) {
						echo $arr_media[$i] . ", ";
					} else {
						echo $arr_media[$i];
					}
				}
				?>
				],
				marker: {
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[1],
					fillColor: 'white'
				}
			}]
		});
		</script>
		<div id="grafico_pontuacao" style="height: 250px; margin: 0 auto"></div>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="tab2">
		<script>
		Highcharts.chart('grafico_precos', {
			chart: {
				backgroundColor: 'transparent'
			},
			title: {text: 'Preços por rodada'},
			colors: ['#26ca5e'],
			tooltip: {
				formatter: function() {
					return '<strong>C$ ' + parseFloat(Math.round(this.y * 100) / 100).toFixed(1) + '</strong>';
				},
				backgroundColor: '#e3672a',
				borderWidth: 0,
				style: {
					color: 'white'
				}
			},
			xAxis: {categories: [
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i != $qtde_rodadas) {
					echo "'" . $arr_rodadas[$i] . "', ";
				} else {
					echo "'" . $arr_rodadas[$i] . "'";
				}
			}
			?>
			]},
			yAxis: {title: {text: 'Preço'}},
			labels: {
				items: [{
					//html: 'Pontos e médias por rodada',
					style: {
						left: '50px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			},
			series: [{
				type: 'column',
				name: 'Preços',
				data: [
				<?php
				for ($i=1; $i<=$qtde_rodadas; $i++) {
					if ($i != $qtde_rodadas) {
						echo $arr_precos[$i] . ", ";
					} else {
						echo $arr_precos[$i];
					}
				}
				?>
				],
				dataLabels: {
					enabled: true,
					//rotation: -90,
					color: '#06a33c',
					align: 'center',
					format: 'C$ {point.y:.1f}', // one decimal
					y: 0, // 10 pixels down from the top
					style: {
						fontSize: '12px',
						fontFamily: 'Arial, sans-serif'
					}
				}
			}]
		});
		</script>
		<div id="grafico_precos" style="height: 250px; margin: 0 auto"></div>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="tab3">
		
		<ul class="nav nav-tabs" role="tablist">
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i == 1) {
					echo '<li role="presentation" class="active">
						<a href="#rodtab'.$i.'" aria-controls="tab1" role="tab" data-toggle="tab">'.$i.'ª rod.</a>
					</li>';
				} else {
					echo '<li role="presentation"><a href="#rodtab'.$i.'" aria-controls="tab1" role="tab" data-toggle="tab">'.$i.'ª rod.</a></li>';
				}
			}
			?>
		</ul>
		
		
		
		<ul id="tabs-swipe-demo" class="tabs">
			<li class="tab col s3"><a href="#test-swipe-1">Test 1</a></li>
			<li class="tab col s3"><a class="active" href="#test-swipe-2">Test 2</a></li>
			<li class="tab col s3"><a href="#test-swipe-3">Test 3</a></li>
		  </ul>
		  <div id="test-swipe-1" class="col s12 blue">Test 1</div>
		  <div id="test-swipe-2" class="col s12 red">Test 2</div>
		  <div id="test-swipe-3" class="col s12 green">Test 3</div>
		
		
		
		<div class="row">
			<div class="col s12 m6 l6 center">
				<script>
				var categories = ['RB', 'FC', 'GC', 'CA', 'CV', 'SG', 'DD', 'DP', 'GS'], count = 0;
						
				Highcharts.chart('grafico_defesa', {

					chart: {
						polar: true
					},

					title: {
						text: 'Defesa'
					},

					xAxis: {
						tickInterval: 1,
						min: 0,
						max: 9,
						labels: {
								formatter: function () {
												var value = categories[count];

									count++;
									if (count == 10) {
											count = 0;
									}

									return value;
							}
						},
					},

					yAxis: {
						min: 0,
						//tickInterval: 5,
						tickPositions: [0, 5, 10],
						minorTickInterval: 0
					},
					
					tooltip: {
						formatter: function () {
							return categories[Highcharts.numberFormat(this.x, 0)] + ': <strong>' + this.y + '</strong>';
						}
					},

					plotOptions: {
						series: {
							pointStart: 0,
							pointInterval: 1
						},
						column: {
							pointPadding: 0,
							groupPadding: 0
						}
					},

					series: [{
						type: 'line',
						name: 'Scout',
						data: [7, 2, 3, 4, 2, 5, 1, 3, 6]
					}]
				});
				</script>
				<div id="grafico_defesa" style="height: 250px; margin: 0 auto"></div>
			</div>
			<div class="col s12 m6 l6 center">
				<script>
				var categories = ['RB', 'FC', 'GC', 'CA', 'CV', 'SG', 'DD', 'DP', 'GS'], count = 0;
						
				Highcharts.chart('grafico_ataque', {

					chart: {
						polar: true
					},

					title: {
						text: 'Ataque'
					},

					xAxis: {
						tickInterval: 1,
						min: 0,
						max: 9,
						labels: {
								formatter: function () {
												var value = categories[count];

									count++;
									if (count == 10) {
											count = 0;
									}

									return value;
							}
						},
					},

					yAxis: {
						min: 0,
						//tickInterval: 5,
						tickPositions: [0, 5, 10],
						minorTickInterval: 0
					},
					
					tooltip: {
						formatter: function () {
							return categories[Highcharts.numberFormat(this.x, 0)] + ': <strong>' + this.y + '</strong>';
						}
					},

					plotOptions: {
						series: {
							pointStart: 0,
							pointInterval: 1
						},
						column: {
							pointPadding: 0,
							groupPadding: 0
						}
					},

					series: [{
						type: 'line',
						name: 'Scout',
						data: [7, 2, 3, 4, 2, 5, 1, 3, 6]
					}]
				});
				</script>
				<div id="grafico_ataque" style="height: 250px; margin: 0 auto"></div>
			</div>
		</div>
		
	</div>
</div>

