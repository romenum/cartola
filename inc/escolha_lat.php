<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_laterais = new stdClass;
$obj_laterais->atleta_id = $atletas_mercado->atleta_id;
$obj_laterais->atleta_apelido = $atletas_mercado->apelido;
$obj_laterais->atleta_foto_140 = $foto_atleta_140;
$obj_laterais->atleta_foto_80 = $foto_atleta_80;
$obj_laterais->atleta_clube = $atletas_mercado->clube_id;
$obj_laterais->status_id = $atletas_mercado->status_id;
$obj_laterais->pontos_num = $atletas_mercado->pontos_num;
$obj_laterais->preco_num = $atletas_mercado->preco_num;
$obj_laterais->variacao_num = $atletas_mercado->variacao_num;
$obj_laterais->media_num = $atletas_mercado->media_num;
$obj_laterais->jogos_num = $atletas_mercado->jogos_num;
if (!empty($atletas_mercado->scout->SG)) {$obj_laterais->scout_saldo = $atletas_mercado->scout->SG;} else {$obj_laterais->scout_saldo = 0;}
if (!empty($atletas_mercado->scout->RB)) {$obj_laterais->scout_roubabola = $atletas_mercado->scout->RB;} else {$obj_laterais->scout_roubabola = 0;}
if (!empty($atletas_mercado->scout->A)) {$obj_laterais->scout_assistencia = $atletas_mercado->scout->A;} else {$obj_laterais->scout_assistencia = 0;}
if (!empty($atletas_mercado->scout->G)) {$obj_laterais->scout_gol = $atletas_mercado->scout->G;} else {$obj_laterais->scout_gol = 0;}

if ($objetivo == 1) { // VALORIZAÇÃO
	if($obj_laterais->jogos_num == 0 && $obj_laterais->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$lateral_valorizar[] = $obj_laterais;
	} else if ($obj_laterais->jogos_num == 1 && $obj_laterais->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$lateral_valorizar[] = $obj_laterais;
	} else if ($obj_laterais->jogos_num == 2 && ($obj_laterais->pontos_num >= 0 && $obj_laterais->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$lateral_valorizar[] = $obj_laterais;
	} else if ($obj_laterais->jogos_num > 2 && $obj_laterais->media_num >= 2 && 
	($obj_laterais->pontos_num >= -3 && $obj_laterais->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$lateral_valorizar[] = $obj_laterais;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_laterais->jogos_num == 0 && ($obj_laterais->preco_num >= 5 && $obj_laterais->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$lateral_pontuar[] = $obj_laterais;
	} else if ($obj_laterais->jogos_num == 1 && ($obj_laterais->preco_num >= 7 && $obj_laterais->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$lateral_pontuar[] = $obj_laterais;
	} else if ($obj_laterais->jogos_num == 2 && ($obj_laterais->scout_saldo/$obj_laterais->jogos_num >= 0.3 
	|| $obj_laterais->scout_roubabola/$obj_laterais->jogos_num >= 2 || $obj_laterais->scout_assistencia/$obj_laterais->jogos_num >= 0.3 || 
	$obj_laterais->scout_gol/$obj_laterais->jogos_num >= 0.3)) { // RODADA 3 DO JOGADOR
		$lateral_pontuar[] = $obj_laterais;
	} else if (($obj_laterais->jogos_num > 2 && $obj_laterais->media_num >= 4) && ($obj_laterais->scout_saldo/$obj_laterais->jogos_num > 0.3 || 
	$obj_laterais->scout_roubabola/$obj_laterais->jogos_num >= 2 || $obj_laterais->scout_assistencia/$obj_laterais->jogos_num >= 0.3 || 
	$obj_laterais->scout_gol/$obj_laterais->jogos_num <= 0.3)) { // RODADA 4 DO JOGADOR EM DIANTE
		$lateral_pontuar[] = $obj_laterais;
	}
}

?>