<?php

foreach ($array_mercado->atletas as $atletas_mercado) {
	if ($atletas_mercado->status_id == 7 && $atletas_mercado->posicao_id == 5 && $atletas_mercado->preco_num <= $peso_jog_ata*$investimento ) {
		include 'inc/escolha_ata.php';
	}
}

if ($objetivo == 1) { // VALORIZAÇÃO
	if (!empty($atacante_valorizar) && count($atacante_valorizar) >= ${'f' . $tipoformacao . '_qtde_ata'}) {
		if ($perfil == 1) {
			usort($atacante_valorizar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($atacante_valorizar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_ata'} . '_atacantes'} = array_slice($atacante_valorizar, 0, ${'f' . $tipoformacao . '_qtde_ata'}, true);
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if (!empty($atacante_pontuar) && count($atacante_pontuar) >= ${'f' . $tipoformacao . '_qtde_ata'}) {
		if ($perfil == 1) {
			usort($atacante_pontuar, "sortByMedia");
		} else if ($perfil == 2) {
			usort($atacante_pontuar, "sortByPreco");
		}
		${'qtde' . ${'f' . $tipoformacao . '_qtde_ata'} . '_atacantes'} = array_slice($atacante_pontuar, 0, ${'f' . $tipoformacao . '_qtde_ata'}, true);
	}
}

if (($objetivo == 1 && !empty($atacante_valorizar) && count($atacante_valorizar) >= ${'f' . $tipoformacao . '_qtde_ata'}) || 
($objetivo == 2 && !empty($atacante_pontuar) && count($atacante_pontuar) >= ${'f' . $tipoformacao . '_qtde_ata'})) {
	for ($i=0; $i < ${'f' . $tipoformacao . '_qtde_ata'}; $i++) {
		// CLUBE DO ATLETA
		foreach ($array_clubes as $clubes) {
			if ((!empty($atacante_valorizar) && $clubes->id == $atacante_valorizar[$i]->atleta_clube) || 
			(!empty($atacante_pontuar) && $clubes->id == $atacante_pontuar[$i]->atleta_clube)) {
				$arr_clube = array (
					'id_clube' => $clubes->id, 'escudo_clube' => end($clubes->escudos), 'nome_clube' => $clubes->nome
				);
				$infos_clube_ata[] = $arr_clube;
			}
		}
	}
}

?>