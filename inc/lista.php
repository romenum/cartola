<?php

$qtde_nao_escalados = 0;
$sigla_posicao = $plural_posicao = "";
for ($i=1; $i<7; $i++) {
	if ($i == 1) { // GOLEIRO
		$sigla_posicao = "gol"; $plural_posicao = "goleiros";
	} else if ($i == 2) { // LATERAL
		$sigla_posicao = "lat"; $plural_posicao = "laterais";
	} else if ($i == 3) { // ZAGUEIRO
		$sigla_posicao = "zag"; $plural_posicao = "zagueiros";
	} else if ($i == 4) { // MEIO-CAMPO
		$sigla_posicao = "mei"; $plural_posicao = "meiocampistas";
	} else if ($i == 5) { // ATACANTE
		$sigla_posicao = "ata"; $plural_posicao = "atacantes";
	} else if ($i == 6) { // TÉCNICO
		$sigla_posicao = "tec"; $plural_posicao = "tecnicos";
	}
	
	// POSIÇÕES
	${'total_' . $plural_posicao} = 0;
	for ($j=0; $j<${'f' . $tipoformacao . '_qtde_' . $sigla_posicao}; $j++) {
		
		if (!empty(${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_apelido)) {
			echo '<div class="col s12">';
				echo '<div class="lista_posicao_atleta col s1">' . ${'posicao' . $i} . '</div>'; 
				echo '<div class="lista_apelido_atleta col s7">' . 
				${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_apelido . '</div>';
				$txt_status_atleta = $color_status_atleta = "";
				if (${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->status_id == 7) {
					$txt_status_atleta = $txt_status_id7; $color_status_atleta = $color_status_id7;
				} else {$txt_status_atleta = $txt_status_id2; $color_status_atleta = $color_status_id2;}
				echo '<div class="lista_status_atleta col s1 ' . $color_status_atleta . '-text">' . $txt_status_atleta . '</div>';
				echo '<div class="lista_preco_atleta col s3"> C$ ' . 
				${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->preco_num .'</div>';
			echo '</div>';
			${'total_' . $plural_posicao} += ${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->preco_num;
		} else {
			$qtde_nao_escalados++;	
		}
	}
}

$total_time = $total_goleiros + $total_laterais + $total_zagueiros + $total_meiocampistas + $total_atacantes + $total_tecnicos;
echo '<div class="col s12"><h5>TOTAL: <strong>C$ '.$total_time.'<strong></h5></div>';

$msg_nao_escalados_ini = '<div class="center card clearfix yellow lighten-2 red-text text-darken-2"><p>Não foi possível escalar ' . $qtde_nao_escalados . ' ';
$msg_nao_escalados_fim = '!<br />Talvez você possua muito pouco dinheiro para as características que escolheu no formulário.</p></div>';
if ($qtde_nao_escalados > 0 && $qtde_nao_escalados <= 1) {
	$txt_jogadores = "jogador";
	echo $msg_nao_escalados_ini . $txt_jogadores . $msg_nao_escalados_fim;
} else if ($qtde_nao_escalados > 1) {
	$txt_jogadores = "jogadores";
	echo $msg_nao_escalados_ini . $txt_jogadores . $msg_nao_escalados_fim;
}

?>