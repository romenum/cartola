<?php

//--------------- VARIÁVEIS SISTEMA - INÍCIO ---------------//
$baseurl = $_SERVER['SERVER_NAME'];
$var_descricao_sistema = "Sistema de escalação automática para o Cartola FC baseado em I.A. (Inteligência Artificial)";

$var_status_mercado_aberto = "Aberto";
$var_status_mercado_fechado = "Fechado";

$var_erro_escolha_sistema_tatico = "Houve algum erro na escolha do sistema tático.";
$var_erro_escolha_maior_invest = "Houve algum erro ao escolher sua área de maior investimento.";

$var_preencher_formulario = "Preencher Formulário";
$var_escolher_opcao = "Escolher opção";
$var_meu_patrimonio_atual = "Meu patrimônio atual";
$var_posso_investir = "Posso investir até:";
$var_quero_economizar = "Quero economizar";
$var_formacao_desejada = "Formação desejada";
$var_area_maior_invest = "Maior investimento";
$var_meu_perfil = "Meu perfil";
$var_meu_objetivo = "Meu objetivo";
$var_obter_escalacao = "Obter Escalação";

$var_dados_preenchidos = "Dados Preenchidos";
$var_patrimonio_atual = "Patrimônio atual:";
$var_valor_para_invest = "Valor para investimento:";
$var_valor_economizado = "Valor economizado:";
$var_formacao_escolhida = "Formação escolhida:";
$var_maior_investimento = $var_area_maior_invest.":";
$var_meu_perfil2 = $var_meu_perfil.":";
$var_objetivo = "Objetivo:";
$var_alterar_dados = "Alterar Dados";

$var_escalacao_campo = "Escalação (campo)";
$var_escalacao_lista = "Escalação (lista)";
$var_escalacao_tabela = "Escalação (tabela)";
$var_area_defesa = "Defesa";
$var_area_meiocampo = "Meio-campo";
$var_area_ataque = "Ataque";
$var_area_igual = "Igualmente";

$var_perfil_conservador = "Conservador";
$var_perfil_equilibrado = "Equilibrado";
$var_perfil_apostador = "Apostador";

$var_objetivo_valorizacao = "Valorização";
$var_objetivo_pontuacao = "Pontuação";
//--------------- VARIÁVEIS SISTEMA - FIM ---------------//


//--------------- VARIÁVEIS CARTOLA - INÍCIO ---------------//
$formacao1 = "3-4-3"; $f1_qtde_gol = 1; $f1_qtde_def = 4; $f1_qtde_lat = 0; $f1_qtde_zag = 3; $f1_qtde_mei = 4; $f1_qtde_ata = 3; $f1_qtde_tec = 1;
$formacao2 = "3-5-2"; $f2_qtde_gol = 1; $f2_qtde_def = 4; $f2_qtde_lat = 0; $f2_qtde_zag = 3; $f2_qtde_mei = 5; $f2_qtde_ata = 2; $f2_qtde_tec = 1;
$formacao3 = "4-3-3"; $f3_qtde_gol = 1; $f3_qtde_def = 5; $f3_qtde_lat = 2; $f3_qtde_zag = 2; $f3_qtde_mei = 3; $f3_qtde_ata = 3; $f3_qtde_tec = 1;
$formacao4 = "4-4-2"; $f4_qtde_gol = 1; $f4_qtde_def = 5; $f4_qtde_lat = 2; $f4_qtde_zag = 2; $f4_qtde_mei = 4; $f4_qtde_ata = 2; $f4_qtde_tec = 1;
$formacao5 = "4-5-1"; $f5_qtde_gol = 1; $f5_qtde_def = 5; $f5_qtde_lat = 2; $f5_qtde_zag = 2; $f5_qtde_mei = 5; $f5_qtde_ata = 1; $f5_qtde_tec = 1;
$formacao6 = "5-3-2"; $f6_qtde_gol = 1; $f6_qtde_def = 6; $f6_qtde_lat = 2; $f6_qtde_zag = 3; $f6_qtde_mei = 3; $f6_qtde_ata = 2; $f6_qtde_tec = 1;
$formacao7 = "5-4-1"; $f7_qtde_gol = 1; $f7_qtde_def = 6; $f7_qtde_lat = 2; $f7_qtde_zag = 3; $f7_qtde_mei = 4; $f7_qtde_ata = 1; $f7_qtde_tec = 1;

$taxa_area_invest = 0.3; // 30% // Taxa para investimento em defesa, meio-campo ou ataque
$peso_tecnico = 0.0595;
$peso_jog_def = $peso_jog_mei = $peso_jog_ata = 0.0855;

$posicao1 = "GOL"; $posicao1txt = "Goleiro";
$posicao2 = "LAT"; $posicao2txt = "Lateral";
$posicao3 = "ZAG"; $posicao3txt = "Zagueiro";
$posicao4 = "MEI"; $posicao4txt = "Meio-Campo";
$posicao5 = "ATA"; $posicao5txt = "Atacante";
$posicao6 = "TEC"; $posicao6txt = "Técnico";

$status_id0 = "?";
$status_id1 = "?";
$status_id2 = "Dúvida"; $txt_status_id2 = '<i class="small material-icons">warning</i>'; $color_status_id2 = "red";
$status_id3 = "Suspenso";
$status_id4 = "?";
$status_id5 = "Contundido";
$status_id6 = "Vazio";
$status_id7 = "Provável"; $txt_status_id7 = '<i class="small material-icons">done</i>'; $color_status_id7 = "green";
//--------------- VARIÁVEIS CARTOLA - FIM ---------------//

?>