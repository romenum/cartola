
<div class="col s12" id="tab1">
	<script>
	Highcharts.chart('grafico_pontuacao', {
		chart: {
			backgroundColor: 'transparent'
		},
		title: {text: 'Pontuação por rodada'},
		colors: ['#26ca5e', '#666666'],
		tooltip: {
			formatter: function() {
				return '<strong>' + parseFloat(Math.round(this.y * 100) / 100).toFixed(1) + ' pontos</strong>';
			},
			backgroundColor: '#e3672a',
			borderWidth: 0,
			style: {
				color: 'white'
			}
		},
		xAxis: {categories: [
		<?php
		for ($i=1; $i<=$qtde_rodadas; $i++) {
			if ($i != $qtde_rodadas) {
				echo "'" . $arr_rodadas[$i] . "', ";
			} else {
				echo "'" . $arr_rodadas[$i] . "'";
			}
		}
		?>
		]},
		yAxis: {title: {text: 'Pontuação'}},
		labels: {
			items: [{
				//html: 'Pontos e médias por rodada',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		series: [{
			type: 'column',
			name: 'Pontos',
			data: [
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i != $qtde_rodadas) {
					echo $arr_pontuacao[$i] . ", ";
				} else {
					echo $arr_pontuacao[$i];
				}
			}
			?>
			],
			dataLabels: {
				enabled: true,
				//rotation: -90,
				color: '#090909',
				align: 'center',
				format: '{point.y:.1f}', // one decimal
				y: 0, // 10 pixels down from the top
				style: {
					fontSize: '12px',
					fontFamily: 'Arial, sans-serif'
				}
			},
			negativeColor: '#ff0320'
		}, {
			type: 'spline',
			name: 'Média',
			data: [
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i != $qtde_rodadas) {
					echo $arr_media[$i] . ", ";
				} else {
					echo $arr_media[$i];
				}
			}
			?>
			],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[1],
				fillColor: 'white'
			}
		}]
	});
	</script>
	<div id="grafico_pontuacao" style="height: 250px; margin: 0 auto"></div>
</div>

<div class="col s12" id="tab2">
	<script>
	Highcharts.chart('grafico_precos', {
		chart: {
			backgroundColor: 'transparent'
		},
		title: {text: 'Preços por rodada'},
		colors: ['#26ca5e'],
		tooltip: {
			formatter: function() {
				return '<strong>C$ ' + parseFloat(Math.round(this.y * 100) / 100).toFixed(1) + '</strong>';
			},
			backgroundColor: '#e3672a',
			borderWidth: 0,
			style: {
				color: 'white'
			}
		},
		xAxis: {categories: [
		<?php
		for ($i=1; $i<=$qtde_rodadas; $i++) {
			if ($i != $qtde_rodadas) {
				echo "'" . $arr_rodadas[$i] . "', ";
			} else {
				echo "'" . $arr_rodadas[$i] . "'";
			}
		}
		?>
		]},
		yAxis: {title: {text: 'Preço'}},
		labels: {
			items: [{
				//html: 'Pontos e médias por rodada',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		series: [{
			type: 'column',
			name: 'Preços',
			data: [
			<?php
			for ($i=1; $i<=$qtde_rodadas; $i++) {
				if ($i != $qtde_rodadas) {
					echo $arr_precos[$i] . ", ";
				} else {
					echo $arr_precos[$i];
				}
			}
			?>
			],
			dataLabels: {
				enabled: true,
				//rotation: -90,
				color: '#090909',
				align: 'center',
				format: 'C$ {point.y:.1f}', // one decimal
				y: 0, // 10 pixels down from the top
				style: {
					fontSize: '12px',
					fontFamily: 'Arial, sans-serif'
				}
			},
			negativeColor: '#ff0320'
		}]
	});
	</script>
	<div id="grafico_precos" style="height: 250px; margin: 0 auto"></div>
</div>

<div class="col s12" id="tab3">
	
	
	<ul class="tabs">
		<?php
		for ($i=1; $i<=$qtde_rodadas; $i++) {
			if ($i == 1) {
				echo '<li class="tab"><a id="fTab" class="active" href="#rodtab'.$i.'">'.$i.'ª rod.</a></li>';
			} else {
				echo '<li class="tab"><a href="#rodtab'.$i.'">'.$i.'ª rod.</a></li>';
			}
		}
		?>
	</ul>
	<?php
	for ($i=1; $i<=$qtde_rodadas; $i++) {
		echo '<div class="col s12" id="rodtab'.$i.'">';
			?>
			
			<div class="row">
				<div class="col s12 m6 l6 center">
					<script>
					Highcharts.chart('grafico_defesa<?php echo $i; ?>', {
						chart: {
							backgroundColor: 'transparent'
						},
						title: {text: 'Defesa'},
						colors: ['#317495'],
						tooltip: {
							backgroundColor: '#e3672a',
							borderWidth: 0,
							style: {
								color: 'white'
							}
						},
						xAxis: {categories: ['RB', 'FC', 'GC', 'CA', 'CV', 'SG', 'DD', 'DP', 'GS']},
						yAxis: {title: {text: 'Quantidade'}},
						labels: {
							items: [{
								//html: 'Pontos e médias por rodada',
								style: {
									left: '50px',
									top: '18px',
									color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
								}
							}]
						},
						series: [{
							type: 'column',
							name: 'Scouts',
							data: [
							<?php
							echo $arr_scout_defesa[$i]["RB"] . ', ' . 
							$arr_scout_defesa[$i]["FC"] . ', ' . 
							$arr_scout_defesa[$i]["GC"] . ', ' . 
							$arr_scout_defesa[$i]["CA"] . ', ' . 
							$arr_scout_defesa[$i]["CV"] . ', ' . 
							$arr_scout_defesa[$i]["SG"] . ', ' . 
							$arr_scout_defesa[$i]["DD"] . ', ' . 
							$arr_scout_defesa[$i]["DP"] . ', ' . 
							$arr_scout_defesa[$i]["GS"];
							?>
							],
							dataLabels: {
								enabled: true,
								//rotation: -90,
								color: '#090909',
								align: 'center',
								y: 0, // 10 pixels down from the top
								style: {
									fontSize: '12px',
									fontFamily: 'Arial, sans-serif'
								}
							},
							negativeColor: '#ff0320'
						}]
					});
					</script>
					<div id="grafico_defesa<?php echo $i; ?>" style="height: 320px; margin: 0 auto"></div>
				</div>
				<div class="col s12 m6 l6 center">
					<script>
					Highcharts.chart('grafico_ataque<?php echo $i; ?>', {
						chart: {
							backgroundColor: 'transparent'
						},
						title: {text: 'Ataque'},
						colors: ['#317495'],
						tooltip: {
							backgroundColor: '#e3672a',
							borderWidth: 0,
							style: {
								color: 'white'
							}
						},
						xAxis: {categories: ['FS', 'PE', 'A', 'FT', 'FD', 'FF', 'G', 'I', 'PP']},
						yAxis: {title: {text: 'Quantidade'}},
						labels: {
							items: [{
								//html: 'Pontos e médias por rodada',
								style: {
									left: '50px',
									top: '18px',
									color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
								}
							}]
						},
						series: [{
							type: 'column',
							name: 'Scouts',
							data: [
							<?php
							echo $arr_scout_defesa[$i]["FS"] . ', ' . 
							$arr_scout_defesa[$i]["PE"] . ', ' . 
							$arr_scout_defesa[$i]["A"] . ', ' . 
							$arr_scout_defesa[$i]["FT"] . ', ' . 
							$arr_scout_defesa[$i]["FD"] . ', ' . 
							$arr_scout_defesa[$i]["FF"] . ', ' . 
							$arr_scout_defesa[$i]["G"] . ', ' . 
							$arr_scout_defesa[$i]["I"] . ', ' . 
							$arr_scout_defesa[$i]["PP"];
							?>
							],
							dataLabels: {
								enabled: true,
								//rotation: -90,
								color: '#090909',
								align: 'center',
								y: 0, // 10 pixels down from the top
								style: {
									fontSize: '12px',
									fontFamily: 'Arial, sans-serif'
								}
							},
							negativeColor: '#ff0320'
						}]
					});
					</script>
					<div id="grafico_ataque<?php echo $i; ?>" style="height: 320px; margin: 0 auto"></div>
				</div>
			</div>
			
			<?php
		echo '</div>';
	}
	?>
	
	
	
</div>

