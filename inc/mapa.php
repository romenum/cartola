<?php

$sigla_posicao = $plural_posicao = "";
for ($i=1; $i<7; $i++) {
	if ($i == 1) { // GOLEIRO
		$sigla_posicao = "gol"; $plural_posicao = "goleiros";
	} else if ($i == 2) { // LATERAL
		$sigla_posicao = "lat"; $plural_posicao = "laterais";
	} else if ($i == 3) { // ZAGUEIRO
		$sigla_posicao = "zag"; $plural_posicao = "zagueiros";
	} else if ($i == 4) { // MEIO-CAMPO
		$sigla_posicao = "mei"; $plural_posicao = "meiocampistas";
	} else if ($i == 5) { // ATACANTE
		$sigla_posicao = "ata"; $plural_posicao = "atacantes";
	} else if ($i == 6) { // TÉCNICO
		$sigla_posicao = "tec"; $plural_posicao = "tecnicos";
	}
	
	// POSICOES
	for ($j=0; $j<${'f' . $tipoformacao . '_qtde_' . $sigla_posicao}; $j++) {
		$cont_jog = $j+1;
		
		if (!empty(${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_id)) {
			if ($i == 1 || $i == 6) {
				echo '<div id="j_' . $sigla_posicao . '" class="jogador_formacao_ok">';
			} else {
				echo '<div id="j_f'.$tipoformacao.'_' . $sigla_posicao . $cont_jog.'" class="jogador_formacao_ok">';
			}

			echo '<a href="#modal_infojogador" data-toggle="modal" 
			data-id="' . ${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_id . '">
				<img id="img_atleta" src="' . ${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_foto_80 . '" 
				title="' . ${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_apelido . '" 
				alt="' .  ${'qtde' . ${'f' . $tipoformacao . '_qtde_' . $sigla_posicao} . '_' . $plural_posicao}[$j]->atleta_apelido . '" /></a>
				<img id="img_clube" src="' . ${'infos_clube_' . $sigla_posicao}[$j]['escudo_clube'] . '" 
				title="' . ${'infos_clube_' . $sigla_posicao}[$j]['nome_clube'] . '" alt="' .  ${'infos_clube_' . $sigla_posicao}[$j]['nome_clube'] . '" />
			</div>';
		}
	}
}

?>

<div id="modal_infojogador" class="modal modal-fixed-footer">
	<div class="modal-header">
		<h5 class="left">Dados do jogador</h5>
	</div>
	<div class="modal-content">
		<div class="fetched-data"></div>
	</div>
		<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn btn-flat">Fechar</a>
	</div>
</div>
