
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Pontuação</a></li>
	<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Cartoletas</a></li>
</ul>

<div class="tab-content">
	<div class="clearfix"></div>
	<div role="tabpanel" class="tab-pane active" id="tab1">
		<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawVisualization);

		function drawVisualization() {
			var data = google.visualization.arrayToDataTable([
				['Rodada', 'Pontuação', 'Média'],
				<?php
				for ($i=1; $i<=$qtde_rodadas; $i++) {
					if ($i != $qtde_rodadas) {
						echo "[" . $arr_pontuacao[$i] . "],";
					} else {
						echo "[" . $arr_pontuacao[$i] . "]";
					}
				}
				?>
			]);
			
			var view = new google.visualization.DataView(data);
			view.setColumns([0, 1, {
				calc: "stringify",
				sourceColumn: 1,
				type: "string",
				role: "annotation"
			}, 2]);
			
			var options = {
				'backgroundColor': 'transparent',
				colors: ['#00827c', '#a91a21'],
				bar: {groupWidth: "98%"},
				legend: { position: "none" },
				title : 'Pontuação por Rodada',
				vAxis: {title: 'Pontos'},
				hAxis: {title: 'Rodadas'},
				seriesType: 'bars',
				series: {1: {type: 'line'}}
			};
			
			var chart = new google.visualization.ComboChart(document.getElementById('chart_pontuacao'));
			chart.draw(view, options);
		}
		</script>
		<div class="col s12">
			<div id="chart_pontuacao" class="chart"></div>
		</div>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="tab2">
		<div class="clearfix"></div>
		<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				['Rodada', 'Preços'],
				<?php
				for ($i=1; $i<=$qtde_rodadas; $i++) {
					if ($i != $qtde_rodadas) {
						echo "[" . $arr_precos[$i] . "],";
					} else {
						echo "[" . $arr_precos[$i] . "]";
					}
				}
				?>
			]);
			
			var view = new google.visualization.DataView(data);
			view.setColumns([0, 1, {
				calc: "stringify",
				sourceColumn: 1,
				type: "string",
				role: "annotation"
			}]);
			
			var options = {
				'backgroundColor': 'transparent',
				colors: ['#00827c'],
				bar: {groupWidth: "98%"},
				legend: { position: "none" },
				title : 'Preço por Rodada',
				vAxis: {title: 'Preço'},
				hAxis: {title: 'Rodadas'},
			};
			
			var chart = new google.visualization.ColumnChart(document.getElementById('chart_precos'));
			chart.draw(view, options);
		}
		</script>
		<div class="col s12">
			<div id="chart_precos" class="chart"></div>
		</div>
	</div>
</div>

