<?php

// FOTO DO ATLETA
$foto_atleta = $atletas_mercado->foto; $foto_atleta_140 = str_replace("FORMATO", "140x140", $foto_atleta);
$foto_atleta = $atletas_mercado->foto; $foto_atleta_80 = str_replace("FORMATO", "80x80", $foto_atleta);

$obj_goleiro = new stdClass;
$obj_goleiro->atleta_id = $atletas_mercado->atleta_id;
$obj_goleiro->atleta_apelido = $atletas_mercado->apelido;
$obj_goleiro->atleta_foto_140 = $foto_atleta_140;
$obj_goleiro->atleta_foto_80 = $foto_atleta_80;
$obj_goleiro->atleta_clube = $atletas_mercado->clube_id;
$obj_goleiro->status_id = $atletas_mercado->status_id;
$obj_goleiro->pontos_num = $atletas_mercado->pontos_num;
$obj_goleiro->preco_num = $atletas_mercado->preco_num;
$obj_goleiro->variacao_num = $atletas_mercado->variacao_num;
$obj_goleiro->media_num = $atletas_mercado->media_num;
$obj_goleiro->jogos_num = $atletas_mercado->jogos_num;
if (!empty($atletas_mercado->scout->SG)) {$obj_goleiro->scout_saldo = $atletas_mercado->scout->SG;} else {$obj_goleiro->scout_saldo = 0;}
if (!empty($atletas_mercado->scout->DD)) {$obj_goleiro->scout_defesa = $atletas_mercado->scout->DD;} else {$obj_goleiro->scout_defesa = 0;}
if (!empty($atletas_mercado->scout->GS)) {$obj_goleiro->scout_gs = $atletas_mercado->scout->GS;} else {$obj_goleiro->scout_gs = 0;}

if ($objetivo == 1) { // VALORIZAÇÃO
	if($obj_goleiro->jogos_num == 0 && $obj_goleiro->preco_num <= 7) { // RODADA 1 DO JOGADOR
		$goleiro_valorizar[] = $obj_goleiro;
	} else if ($obj_goleiro->jogos_num == 1 && $obj_goleiro->variacao_num >= 3) { // RODADA 2 DO JOGADOR
		$goleiro_valorizar[] = $obj_goleiro;
	} else if ($obj_goleiro->jogos_num == 2 && ($obj_goleiro->pontos_num >= 0 && $obj_goleiro->pontos_num <= 3)) { // RODADA 3 DO JOGADOR
		$goleiro_valorizar[] = $obj_goleiro;
	} else if ($obj_goleiro->jogos_num > 2 && $obj_goleiro->media_num >= 2 && 
	($obj_goleiro->pontos_num >= -3 && $obj_goleiro->pontos_num <= 2)) { // RODADA 4 DO JOGADOR EM DIANTE
		$goleiro_valorizar[] = $obj_goleiro;
	}
} else if ($objetivo == 2) { // PONTUAÇÃO
	if($obj_goleiro->jogos_num == 0 && ($obj_goleiro->preco_num >= 5 && $obj_goleiro->preco_num <= 10)) { // RODADA 1 DO JOGADOR
		$goleiro_pontuar[] = $obj_goleiro;
	} else if ($obj_goleiro->jogos_num == 1 && ($obj_goleiro->preco_num >= 7 && $obj_goleiro->preco_num <= 12)) { // RODADA 2 DO JOGADOR
		$goleiro_pontuar[] = $obj_goleiro;
	} else if ($obj_goleiro->jogos_num == 2 && ($obj_goleiro->scout_saldo/$obj_goleiro->jogos_num >= 0.3 || 
	$obj_goleiro->scout_defesa/$obj_goleiro->jogos_num >= 2 || $obj_goleiro->scout_gs/$obj_goleiro->jogos_num <= 0.6)) { // RODADA 3 DO JOGADOR
		$goleiro_pontuar[] = $obj_goleiro;
	} else if (($obj_goleiro->jogos_num > 2 && $obj_goleiro->media_num >= 4) && 
	($obj_goleiro->scout_saldo/$obj_goleiro->jogos_num >= 0.3 || $obj_goleiro->scout_defesa/$obj_goleiro->jogos_num >= 1.3 || 
	$obj_goleiro->scout_gs/$obj_goleiro->jogos_num <= 0.6)) { // RODADA 4 DO JOGADOR EM DIANTE
		$goleiro_pontuar[] = $obj_goleiro;
	}
}

?>